function [trial_aligned_Python_eye_data] = GetTrialAlignedPythonGazeData(session_data)
%Written by Seth Koenig 3/8/19
%Coppied from other analysis code, just mades sense to make standalone
%function since we use it all the time and there will be modifications we
%want to make as we improve this and change the eye tracking bridge

fSample = 600;
valid_data_trials = 0;
num_trials = session_data.num_trials;
trial_aligned_Python_eye_data = cell(1,num_trials);

if isempty(session_data.gaze_data)
    disp(['No gaze data found for ' session_data.filename]);
    return
end
for t = 1:num_trials
    
    
    ITI_epoch_start = [session_data.trial_data.Epoch0_StartFrame(t) session_data.trial_data.Epoch0_StartTimeAbsolute(t) session_data.trial_data.Epoch0_StartTimeRelative(t)];
    Reward_epoch_start =  [session_data.trial_data.Epoch7_StartFrame(t) session_data.trial_data.Epoch7_StartTimeAbsolute(t) session_data.trial_data.Epoch7_StartTimeRelative(t)];
    Reward_epoch_duration = session_data.trial_data.Epoch7_Duration(t);
    
    this_trial_ind = find(session_data.frame_data.TrialInExperiment == t);
    reward_epoch_end_frame = session_data.frame_data.Frame(this_trial_ind(end));
    
    trial_frame_start = ITI_epoch_start(1);
    trial_end_frame = reward_epoch_end_frame;
    
    trial_Unity_time_start = ITI_epoch_start(2);
    trail_Unity_end_time = Reward_epoch_start(2)+Reward_epoch_duration;
    
    UDP_start_index = find(session_data.UDP_recv_data.Frame == trial_frame_start);
    if length(UDP_start_index) > 1
        if all(diff(UDP_start_index)) == 1
            UDP_start_index = UDP_start_index(1);
            disp('Found Multiple UDP Start Messages in a row, taking the 1st one')
        else
            error('Found Multiple UDP start Messages, and they are not in a row')
        end
    elseif any(abs(session_data.UDP_recv_data.Frame - trial_frame_start) == 1)
        UDP_start_index = find(abs(session_data.UDP_recv_data.Frame - trial_frame_start) == 1);
        UDP_start_index = UDP_start_index(1);
    elseif isempty(UDP_start_index)
        error('Cant find Trial Start Frame in UDP')
    end
    
    
    UDP_end_index = find(session_data.UDP_recv_data.Frame == trial_end_frame);
    if length(UDP_end_index) > 1
        if all(diff(UDP_end_index)) == 1
            UDP_end_index = UDP_end_index(1);
            disp('Found Multiple UDP End Messages in a row, taking the 1st one')
        else
            error('Found Multiple UDP End Messages, and they are not in a row')
        end
    elseif isempty(UDP_end_index)
        if t == num_trials
            if abs(session_data.UDP_recv_data.Frame(end)-trial_end_frame) <= 1
                UDP_end_index = length(session_data.UDP_recv_data.Frame);
                trial_end_frame = session_data.UDP_recv_data.Frame(end);
            else
                error('Cant find Trial Start Frame in UDP')
            end
        elseif any(abs(session_data.UDP_recv_data.Frame - trial_end_frame) == 1)
            UDP_end_index = find(abs(session_data.UDP_recv_data.Frame - trial_end_frame) == 1);
            UDP_end_index = UDP_end_index(1);
        else
            error('Cant find Trial Start Frame in UDP')
        end
    end
    
    %verify frame start time is within 1 frame across files
    %large spikes in inter-frame time can happen due to loading Quaddles
    if abs(session_data.UDP_recv_data.FrameStartUnity(UDP_start_index)- trial_Unity_time_start) > 0.1
        disp(['Trial Start: UDP and Frame Start times are off :( by ' ...
            num2str(abs(session_data.UDP_recv_data.FrameStartUnity(UDP_start_index)- trial_Unity_time_start))])
    end
    if abs(session_data.UDP_recv_data.FrameStartUnity(UDP_end_index)- trail_Unity_end_time) > 0.1
        disp(['Trial End: UDP and Frame Start times are off :( by ' ...
            num2str(abs(session_data.UDP_recv_data.FrameStartUnity(UDP_end_index)- trail_Unity_end_time))])
    end
    
    Eye_tracker_time_stamp_start = session_data.UDP_recv_data.EyeTrackerTimeStamp(UDP_start_index);
    if isnan(Eye_tracker_time_stamp_start)
        if UDP_start_index ~= 1
            prior_time_stamp = session_data.UDP_recv_data.EyeTrackerTimeStamp(UDP_start_index-1);
        else
            prior_time_stamp = session_data.UDP_recv_data.EyeTrackerTimeStamp(UDP_start_index+1);
        end
        next_time_stamp = session_data.UDP_recv_data.EyeTrackerTimeStamp(UDP_start_index+1);
        Eye_tracker_time_stamp_start = (next_time_stamp + prior_time_stamp)/2;
        session_data.UDP_recv_data.EyeTrackerTimeStamp(UDP_start_index) = Eye_tracker_time_stamp_start;
        disp(['Trial #' num2str(t) ', Started ET is a NaN, corrected by interpreting between time points']);
    elseif isempty(Eye_tracker_time_stamp_start)
        error('What, Trial Start ET is Empty')
    end
    
    Eye_tracker_time_stamp_end = session_data.UDP_recv_data.EyeTrackerTimeStamp(UDP_end_index);
    if isnan(Eye_tracker_time_stamp_end)
        if ~isnan(session_data.UDP_recv_data.EyeTrackerTimeStamp(UDP_end_index+1))
            Eye_tracker_time_stamp_end = session_data.UDP_recv_data.EyeTrackerTimeStamp(UDP_end_index+1);
        elseif  ~isnan(session_data.UDP_recv_data.EyeTrackerTimeStamp(UDP_end_index-1))
            Eye_tracker_time_stamp_end = session_data.UDP_recv_data.EyeTrackerTimeStamp(UDP_end_index-1);
        else
            error('What, Trial End ET is a NaN')
        end
    elseif isempty(Eye_tracker_time_stamp_end)
        error('What, Trial End ET is Empty')
    end
    
    python_ind  = find(session_data.gaze_data.EyeTrackerTimeStamp >= Eye_tracker_time_stamp_start & ...
        session_data.gaze_data.EyeTrackerTimeStamp <= Eye_tracker_time_stamp_end);
    
    trial_aligned_Python_eye_data{t}.PythonTimeStamp = session_data.gaze_data.PythonTimeStamp(python_ind,:);
    trial_aligned_Python_eye_data{t}.EyeTrackerTimeStamp = session_data.gaze_data.EyeTrackerTimeStamp(python_ind,:);
    trial_aligned_Python_eye_data{t}.Lx = session_data.gaze_data.Lx(python_ind,:);
    trial_aligned_Python_eye_data{t}.Ly = session_data.gaze_data.Ly(python_ind,:);
    trial_aligned_Python_eye_data{t}.Rx = session_data.gaze_data.Rx(python_ind,:);
    trial_aligned_Python_eye_data{t}.Ry = session_data.gaze_data.Ry(python_ind,:);
    trial_aligned_Python_eye_data{t}.PupilL = session_data.gaze_data.PupilL(python_ind,:);
    trial_aligned_Python_eye_data{t}.PupilR = session_data.gaze_data.PupilR(python_ind,:);
    
    %% Clean up noise and combine L/R Gaze Data
    [trial_aligned_Python_eye_data{t}.X,trial_aligned_Python_eye_data{t}.Y,trial_aligned_Python_eye_data{t}.Pupil] = ...
        Cleaned_and_combined_gaze(trial_aligned_Python_eye_data{t}.Lx,trial_aligned_Python_eye_data{t}.Ly,...
        trial_aligned_Python_eye_data{t}.Rx,trial_aligned_Python_eye_data{t}.Ry,...
        trial_aligned_Python_eye_data{t}.PupilL,trial_aligned_Python_eye_data{t}.PupilR,fSample);
    
    %% Detect Saccades
    valid_gaze_data = ~isnan(trial_aligned_Python_eye_data{t}.X) & ~isnan(trial_aligned_Python_eye_data{t}.Y);
    if sum(valid_gaze_data) > 500
        sacInd = madSaccade(trial_aligned_Python_eye_data{t}.X,trial_aligned_Python_eye_data{t}.Y,fSample);
        valid_data_trials = valid_data_trials + 1;
        [~,sac_start,sac_end] = findgaps(find(sacInd == 1));
    elseif sum(valid_gaze_data)  == 0
        sac_start = [];
        sac_end  = [];
    elseif session_data.trial_data.TrialTime(t) > 10%probably a time out
        sac_start = [];
        sac_end  = [];
    else
        error('Why is there so little gaze data?')
    end
    trial_aligned_Python_eye_data{t}.saccadetimes = [sac_start; sac_end];
    %%
end
disp([num2str(valid_data_trials) '/' num2str(num_trials) ' with enough gaze data'])
end