function data = ImportFLU_Data(filename,data_dir)

if nargin < 2
    log_dir = FindDataDirectory(filename);
else
    log_dir = data_dir;
end

trial_dir = [log_dir filename '\RuntimeData\TrialData\'];
trial_name = '__TrialData.txt';
trialdef_name = '__trialdef_on_trial_';

config_dir = [log_dir filename '\RuntimeData\ConfigCopy\'];
block_name = '__block_defs.json';

trial_data = readtable([trial_dir filename trial_name]);

if trial_data(end,:).Epoch4_Duration == -1
    %last trial was never completed
    trial_data(end,:) = [];
end
block_def = ReadJsonFile([config_dir filename block_name]);

num_trials = size(trial_data,1);
TrialDefs = cell(1,num_trials);
for td = 1:num_trials
    TrialDefs{td} = ReadJsonFile([trial_dir filename trialdef_name num2str(td) '.json']);
end

%Export as structure Array
data.trial_data = trial_data;
data.block_def = block_def;
data.trial_def = TrialDefs;

end