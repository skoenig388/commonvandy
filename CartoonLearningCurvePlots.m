%Code makes cartoon plots for learning curves and search efficiency

clar
easy = 0.5;
medium = 0.35;
hard = 0.2;
reversal = 0.1;

expo1 = 95*(1-1.1*exp(-easy*(1:30)));
performance1 = [95*ones(1,10) expo1];

expo2 = 95*(1-exp(-medium*(1:30)));
performance2 = [95*ones(1,10) expo2]-1;

expo3 = 95*(1-0.8*exp(-hard*(1:30)));
performance3 = [95*ones(1,10) expo3]-2;

expo4 = 95*(1-1.19*exp(-hard*(1:30)));
performance4 = [95*ones(1,10) expo4]-3;

figure
plot([-9 30],[33 33],'k')
hold on
plot([0.5 0.5],[0 100],'k--')
p(1) = plot(-9:30,performance1);
p(2) = plot(-9:30,performance2);
p(3) = plot(-9:30,performance3);
p(4) = plot(-9:30,performance4);
hold off
xlim([-9 30])
ylim([0 100])
box off
xticks([])
yticks([])
xlabel('Time from Reward-Rule Change')
ylabel('Performance')
legend(p,{'Easy','Medium','Hard','Reversal'},'location','SouthEast')

%%

figure
hold on
plot([0.5 0.5],[0 100],'k--')
p(1) = plot(-9:30,100-performance1);
p(2) = plot(-9:30,100-performance2);
p(3) = plot(-9:30,100-performance3);
p(4) = plot(-9:30,100-performance4);
hold off
xlim([-9 30])
ylim([0 100])
box off
xticks([])
yticks([])
xlabel('Time from Reward-Rule Change')
ylabel('Search Duration')
legend(p,{'Easy','Medium','Hard','Reversal'},'location','NorthEast')
