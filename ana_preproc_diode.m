% n = 10000;
% a=cell(n,1);
% [a{:}]=deal('12B');
% tic
% b1=hex2dec(a);
% toc
% tic
% b2=cell(size(a));
% for ii=1:numel(a)
%    b2{ii}=hex2dec(a{ii}); 
% end
% toc
% size(b1)
% return



if 1
    %testing hospital data
    if 0
        path = '/Users/ben/Desktop/timing_blitz/subjectDataWserial.mat';
        load(path)
        dat = subjectData.Runtime.SerialData.JoyAndPhotoDetails;
    end

    % load in new data
    path = '/Users/ben/Desktop/testing_diodes';
    % name = 'NARD__Subject0__21_08_2018__16_36_20';
%     name = 'NARD__Subject0__21_08_2018__17_05_24';
%     name = 'NARD__Subject0__21_08_2018__17_56_16';
%     name = 'NARD__Subject0__21_08_2018__18_10_38';
%     name = 'NARD__Subject0__21_08_2018__19_16_20'; %overnight test 1
%     name = 'NARD__Subject0__22_08_2018__16_29_29'; %overnight 2

%     name = 'NARDGAME__Subject0__23_08_2018__12_54_48'; %play through 8-10 min
%     name = 'NARDSMOKE__Subject0__23_08_2018__13_16_33'; %smoke test
%     name = 'NARDBUILD__Subject0__27_08_2018__12_41_43'; % 10 min of built game
    name = 'NARDSMOKE_LR__Subject0__27_08_2018__13_17_00'; %static discharge
%     name = 'NARDPLAY__Subject0__27_08_2018__13_56_27';
%     name = 'NARDNIGHT__Subject0__27_08_2018__18_08_11'; %night test 2
    
    str = [path '/' name '/RuntimeData/UnitySerialRecvData'];
%     str = [path '/' name '/RuntimeData/UnitySerialSentData'];
    cd(str)

    sname = [path '/' name '/processedData.mat'];

    if 1
        d = dir([name '*Trial*']);
        [~,id] = sort_nat({d.name});
        d = d(id);
        
%         error('have to append data with natsort')

        rawData = [];
        for id=1:numel(d)
            disp(id)
            tmp = readtable(d(id).name);

            if id==1
                rawData = tmp;
            else
                rawData = cat(1,rawData,tmp);
            end
        end

        % howmuch data should we take?
%         tt = 0.4;
%         b = rawData.FrameStart;
%         ii = round( (tt*60*60) / mean(diff(b)) );
%         rawData(ii:end,:) = [];

%         tic
%         parsedData = ParseNeurarduinoData(rawData);
%         t1 = toc;
%         toc
%         tic
%         parsedData2 = ParseNeurarduinoData2(rawData);
%         t2 = toc;
%         toc
        tic
        parsedData = ParseNeurarduinoData2(rawData);
        dat = parsedData.JoyAndPhotoDetails;
        toc
        
        tic
        parsedDat3 = ParseNeurarduinoData3(rawData);
        dat = parsedDat3.JoyAndPhotoDetails;
        toc

        %save
        save(sname,'parsedData','dat','-v7.3')

%         try
%             save(sname,'rawData','parsedData','dat')
%         catch
%             try
%                 save(sname,'rawData','parsedData','dat','-v7.3')
%             catch
%                 save(sname,'parsedData','dat','-v7.3')
%             end
%         end
    else
       load(sname) 
    end
end

% p = sum(abs

% when are commands sent out?
t = dat.ArduinoTimestamp * 10^-3;
dt = diff(t);

bad = find(dt > 0.07);
iframe = dat.FrameStart(bad+1);

ff = parsedData.SystemStatus.FrameStart;
isystem = ff( cellfun(@(x) nearest(ff,x),num2cell(iframe)) );

d = iframe - isystem;

% return
% plot stuff
st_orig = 1;%83;
l = dat.PhotoL(st_orig:end); %flip every frame
r = dat.PhotoR(st_orig:end); %coded flip

if 1
    t = dat.ArduinoTimestamp * 10^-3;
    t = t-t(1);
%     t = t / 60; %minutes
    xstr = 'time';
else
    t = 1:numel(l);
    xstr = 'samples';
end


% looking for artifacts
len = round(200/3.3);
win = ones(1,len) ./ len;

ord = 4;
fs = 1/0.0033;
fc1 = (1/0.1);
fc2 = (1/0.5);
[b,a] = butter(ord,fc1./(fs/2),'low');
l2 = filtfilt(b,a,l);
[b,a] = butter(ord,fc2./(fs/2),'low');
r2 = filtfilt(b,a,r);
        
% l2 = conv(l,win,'same');
% r2 = conv(r,win,'same');

if 0
    figure;
    nr = 2; nc =1;

    subplot(nr,nc,1)
    plot(t,l,'k.-')
    hold all
    plot(t,l2,'r-')
    title('left')
    legend({'raw',sprintf('BP, fc=%g',fc1)})

    subplot(nr,nc,2)
    plot(t,r,'k.-')
    hold all
    plot(t,r2,'r-')
    title('right')
    legend({'raw',sprintf('BP, fc=%g',fc2)})


    set_bigfig(gcf,[0.9,0.9])
end

%plot some potential artifacts
d = diff(l2);

figpath = [path '/' name '/_fig'];
if ~exist(figpath); mkdir(figpath); end

thresh = 8;
mpd = round(0.1/0.0033);
[~,ipk] = findpeaks(abs(d),'minpeakheight',thresh,'minpeakdistance',mpd);


pad = round(3*60/0.0033);
for ii=1:numel(ipk)
    s = sprintf('%g/%g',ii,pad);
    disp(s)
   figure('visible','off') 
   nr = 2;
   nc = 1;
   
   pad1 = round(2/0.0033);
   pad2 = round(10/0.0033);
   ind = max(ipk(ii)-pad1,1) : min(ipk(ii)+pad2,numel(t));
   
  tt = t(ind)-t(ind(1));
  strs = {'left','right'};
   for jj=1:2
        if jj==1
            x1 = l(ind);
            x2 = l2(ind);
            fc = fc1;
        else
            x1 = r(ind);
            x2 = r2(ind);
            fc = fc2;
        end
       
        subplot(nr,nc,jj)
        plot(tt,x1,'k.-')
        hold all
        plot(tt,x2,'r-')

        set(gca,'xlim',[tt(1),tt(end)])

        s = sprintf('%s diode, time=%.5g',strs{jj},t(ipk(ii)));
        title(s,'fontsize',14)
        xlabel('time(s)')  
        legend({'raw',sprintf('BP, fc=%g',fc)})

   end

   set_bigfig(gcf,[0.7,1])
   
   %save
   sname = sprintf('%s/%g',figpath,ii);
   print(sname,'-dpng')
   close gcf
   
end



% example plots
return
figure
nr = 2; nc = 1;
hax = []; p = [];

subplot(nr,nc,1)
p(1) = plot(t,l,'k.-');
hax(1) = gca;
title('left')
xlabel(xstr)

subplot(nr,nc,2)
p(2) = plot(t,r,'k.-');
hax(2) = gca;
title('right')
xlabel(xstr)

set_bigfig(gcf,[0.8 0.8])

% for smoke test: NARDSMOKE_LR__Subject0__27_08_2018__13_17_00
tests = {
    'L',13;
    'L',27;
    'L',58;
    'R',24;
    'R',47;
    'R',60+9;
};

flag = 1;
fn = 0;
pad = 1000;

ah = [];
while flag 
    st = fn+1;
    fn = min(numel(t),st+pad);

    if st > numel(l)
        flag = 0;
    else
        
%         tt = t(st:fn)-t(st);
        tt = t(st:fn);
        set(p(1),'XData',tt, 'YData',l(st:fn))
        set(p(2),'XData',tt, 'YData',r(st:fn))
        
        %label
        N = [tests{:,2}];
        sel = cellfun(@(x) any( x>=tt(1) & x<=tt(end)),num2cell(N));
%         sel = N>=tt(1) & N<=tt(2);
        
        if 0 %any(sel)
            label = tests(sel,1);
            nt = N(sel);
            h = plotcueline('x',nt);

            yy = get(gca,'ylim');
            for ih=1:numel(h)
                text(nt(ih),yy(2),label{ih})
%                 ah = annotation('textbox',[]);
%                 ah.Text = label{ih};
            end
            
            set(hax,'xlim',[tt(1), tt(end)])
            pause
        elseif 1
            pause
        end
    end
end


