function JsonData = ReadJsonFile(filename)

fid = fopen(filename);
raw = fread(fid,inf);
str = char(raw');
fclose(fid);
JsonData = jsondecode(str);
