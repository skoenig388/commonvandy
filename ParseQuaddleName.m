function [dimVals,neutralVals] = ParseQuaddleName(stim_name,color_names,arm_names)

if nargin == 3
    dimVals = NaN(1,5);
else
    dimVals = NaN(2,5);
end
neutralVals = NaN(1,5);

%first parse name
shape_value = str2double(stim_name(strfind(stim_name,'S')+1:strfind(stim_name,'S')+2));
pattern_value = str2double(stim_name(strfind(stim_name,'P')+1:strfind(stim_name,'P')+2));
texture_value = str2double(stim_name(strfind(stim_name,'T')+1:strfind(stim_name,'T')+2));

color_value1 = str2double(stim_name(strfind(stim_name,'C')+1:strfind(stim_name,'C')+7));
color_value2 = str2double(stim_name(strfind(stim_name,'C')+9:strfind(stim_name,'C')+15));

arm_angle_value = str2double(stim_name(strfind(stim_name,'A')+1:strfind(stim_name,'A')+2));
arm_end_values = str2double(stim_name(strfind(stim_name,'E')+1:strfind(stim_name,'E')+2));


dimVals(1,1) = shape_value;
if shape_value == 0
    neutralVals(1) = true;
else
    neutralVals(1) = false;
end

dimVals(1,2) = pattern_value;
if pattern_value == 0
    neutralVals(2) = true;
else
    neutralVals(2) = false;
end

if nargin == 3
    color_ind = find(contains(color_names,stim_name(strfind(stim_name,'C'):strfind(stim_name,'C')+15)));
    if ~isempty(color_ind)
        dimVals(1,3) = color_ind;
    else
        dimVals(1,3) = 0;
    end
else
    dimVals(1,3) = color_value1;
    dimVals(2,3) = color_value2;
end

if  (color_value1 == 7000000 || color_value1 == 6000000 || color_value1 == 5000000) && ...
        (color_value2 == 7000000 || color_value2 == 6000000 || color_value2 == 5000000)
    neutralVals(3) = true;
else
    neutralVals(3) = false;
end

dimVals(1,4) = texture_value;
if texture_value == 0
    neutralVals(4) = true;
else
    neutralVals(4) = false;
end

if nargin == 3
    arm_ind = find(contains(arm_names,stim_name(strfind(stim_name,'A'):strfind(stim_name,'E')+2)));
    if ~isempty(arm_ind)
        dimVals(1,5) = arm_ind;
    else
        dimVals(1,5) = 0;
    end
else
    dimVals(1,5) = arm_angle_value;
    dimVals(2,5) = arm_end_values;
end
if arm_angle_value == 0 && arm_end_values == 0
    neutralVals(5) = true;
else
    neutralVals(5) = false;
end

end