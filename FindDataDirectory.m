function log_dir = FindDataDirectory(filename)
%Written by Seth Koenig 3/8/19
%Code searches through possibly data directories to find files when data
%directory was not specified.

log_dir = [];
% tic;

which_monkey = [];
if contains(filename,'Bard')
    which_monkey = 'Bard';
elseif contains(filename,'Frey')
    which_monkey = 'Frey';
elseif contains(filename,'Igor')
    which_monkey = 'Igor';
elseif contains(filename,'Reider')
    which_monkey = 'Reider';
elseif contains(filename,'Sindri')
    which_monkey = 'Sindri';
elseif contains(filename,'Wotan')
    which_monkey = 'Wotan';
elseif contains(filename,'MFLU')
    which_monkey = 'Human';
else
    error('Did not recognize this monkey')
end

base_dir = [];
if exist(['Z:\MonkeyData\' which_monkey '\'],'dir') && exist(['Z:\DATA_kiosk\' which_monkey '\'],'dir')
    base_dir{1} = ['Z:\MonkeyData\' which_monkey '\'];
    base_dir{2} = ['Z:\DATA_kiosk\' which_monkey '\'];
elseif exist(['Z:\MonkeyData\' which_monkey '\'],'dir')
    base_dir{1} = ['Z:\MonkeyData\' which_monkey '\'];
elseif exist(['Z:\DATA_kiosk\' which_monkey '\'],'dir')
    base_dir{1} = ['Z:\DATA_kiosk\' which_monkey '\'];
else
    error('General File Location Unknown');
end

task_folders = {'Context Feature Search Task\','FLU2FS\','FLU Task\',...
    'Feature Search Task\','Fixation\','VDS\','VDM\','Touch-Hold-Release Task\'};
task_names = {'CFS','FLU2FS','FLU','FS','Fixation','VDS','VDM','THR'};

which_task = [];
for task = 1:length(task_names)
    if contains(filename,task_names{task})
        which_task = task;
        break
    end
end

%try to find file in task folder with same task name but file names could be wrong
if exist([base_dir{1} task_folders{task} filename],'dir')
    log_dir = [base_dir{1} task_folders{task}];
    %     t2 = toc;
    %     disp(['Took ' num2str(t2) ' to find file'])
    return
elseif length(base_dir) == 2
    if exist([base_dir{2} task_folders{task} filename],'dir')
        log_dir = [base_dir{2} task_folders{task}];
        %         t2 = toc;
        %        disp(['Took ' num2str(t2) ' to find file'])
        return
    end
end

%do full search across all directories possible
for b = 1:length(base_dir)
    for td = 1:length(task_folders)
        if exist([base_dir{b} task_folders{td} filename],'dir')
            log_dir = [base_dir{b} task_folders{td}];
            %             t2 = toc;
            %            disp(['Took ' num2str(t2) ' to find file'])
        end
    end
end

if isempty(log_dir)
    disp(['Could not find specified file: ' filename])
end
% t2 = toc;
%disp(['Took ' num2str(t2) ' to find file'])
end