function [Combined_X,Combined_Y,Combined_Pupil] = Cleaned_and_combined_gaze(Lx,Ly,Rx,Ry,PupilL,PupilR,fsample)
%Written by Seth Konig 7/19/19 based on code from Ben Voloh

%Inputs:
% 1) Lx: Horizontal gaze data from left eye
% 2) Ly: Vertical gaze data from left eye
% 3) Rx: Horizontal gaze data from rigth eye
% 4) Ry: Vertical gaze data from right eye
% 5) PupilL: Pupil data from left eye
% 6) PupilR: Pupil data from Right eye
% 7) fsample: Eye tracker sampling rate e.g. 600 Hz

%***Lx, Ly, Rx, and Ry shouul be in ADCS (normlaized screen space)
%***PupilL/PupilR do not have to be in particular coordinate systems since
%can be arbiratry

%Outputs:
% 1) Combined_X: Cleaned up Horizontal gaze data combined from Lx/Rx
% 2) Combined_Y: Cleaned up Vertical gaze data combined from Ly/Ry
% 3) Combined_Pupil: Cleaned up Pupil data combined from PupilL/PupilR

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%---Main Parameters---%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%
warning('off','MATLAB:polyfit:RepeatedPointsOrRescale');%supress annoying warning

plot_results = false;%if true, will generate several plots of processing steps

%noise filter parameters
lambda = 6;%sensitivity for mad-based thresholds
blank_samples = ceil(0.01*fsample);%number of samples to blank at signal edges & spikes if noise is above lambda threshold
median_spike_filter_width = 3;%median filter for single spike removal, should be 3 by default

%sparsity parameters
timesparse_window = 0.04;%time length (in seconds) of chuncks to look at for sparsity
maxNanTime = timesparse_window * 0.25;%time length (in seconds) of allowable data without samples
minNanTime = maxNanTime*fsample/2;%max amount of time to interpolate missing data
sparse_thresh = maxNanTime ./ timesparse_window;%threshold for proportion of missing data (nan) that is acceptable
winlen = ceil(timesparse_window*fsample);
if mod(winlen,2)==0
    winlen=winlen+1;
end
sparse_wind = ones(1,winlen);

%minimum data length parameters
time_too_short = ceil(0.1*fsample);%how short of data is valid, anything shorter may not be very useful

%off screen parameters
xy_max_thresh = 2;%1x of screen in positive direction i.e. 2x screen width
xy_min_thresh = -1;%1x off screen in negative direction

%disparity parameters
disparity_window = time_too_short;%time window to look at around missing disparity measures
gaze_disparity_thresh = 0.1;%proportion of data where disparity is too great to consider
disparity_medial_filter_width = 11;%median filter width for estimating disparity between L/R eyes, can be more liberal here

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%---Format Gaze Data in to Matrix So To Loop Through---%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cleaned_gaze_data = cell(2,3);%finally cleaned data to store
gaze_data = cell(2,3);

gaze_data{1,1} = Lx;
gaze_data{1,2} = Ly;
gaze_data{2,1} = Rx;
gaze_data{2,2} = Ry;

gaze_data{1,3} = PupilL;
gaze_data{2,3} = PupilR;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%-Removed Data way off Screen---%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%doesn't apply to pupil
for lr = 1:2
    these_gaze_data_x = gaze_data{lr,1};
    these_gaze_data_y = gaze_data{lr,2};
    
    outside_x = find(these_gaze_data_x > xy_max_thresh | these_gaze_data_x < xy_min_thresh);
    outside_y = find(these_gaze_data_y > xy_max_thresh | these_gaze_data_y < xy_min_thresh);
    outside_xy = union(outside_x,outside_y);
    
    these_gaze_data_x(outside_xy) = NaN;
    these_gaze_data_y(outside_xy) = NaN;
    
    gaze_data{lr,1} = these_gaze_data_x;
    gaze_data{lr,2} = these_gaze_data_y;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%---Cleanup Sparse Data---%
%%%%%%%%%%%%%%%%%%%%%%%%%%%
%1) Remove data that is sparsely sampled determined by maxNanTime
%2) Interpolate missing data, max # of sample to interpolated defined by minNanTime
%3) Remove data that is too short to matter, min legnth set by timesparse_window
%4) Remove edges of good data which often is associated with large abrupt signal changes
%5) Remove single sample spikes using median filtering
%6) Remove noisy samples by replacing noisy samples with samples if they were over smoothed
for lr = 1:2
    for xyp = 1:3
        
        temp_eye = gaze_data{lr,xyp};
        if all(isnan(temp_eye))
            cleaned_gaze_data{lr,xyp} = gaze_data{lr,xyp};
            continue
        end
        
        %---#1: Remove Poorly Sampled Locations---%
        %find data points with NaNs
        sparse_bad_ind = isnan(temp_eye);
        sparse_bad_ind = double(sparse_bad_ind);
        N = conv(ones(size(sparse_bad_ind)),sparse_wind,'same');
        sparse_bad = conv(sparse_bad_ind,sparse_wind,'same');
        sparse_bad = sparse_bad ./ N;
        
        
        %remove sparse periods
        %minimum segement length should take care of later if segments are
        %too short, but short sparse periods could be interpolatable later
        sparsePeriods = sparse_bad >= sparse_thresh;
        sparsePeriods_shortened = sparsePeriods;
        spared_periods = findgaps(find(sparsePeriods_shortened == 1));
        for sp = 1:size(spared_periods,1)
            sps = spared_periods(sp,:);
            sps(sps == 0) = [];
            if length(sps) < ceil(time_too_short/2)
                sparsePeriods_shortened(sps(1):sps(end)) = 0;
            end
        end
        
        temp_eye_sparsed = temp_eye;
        temp_eye_sparsed(sparsePeriods_shortened) = NaN;
        
        if all(isnan(temp_eye_sparsed))
            cleaned_gaze_data{lr,xyp} = temp_eye_sparsed;
            continue
        end
        
        %---#2: Interpolate missing data---%
        %interpolate any short missing pieces
        nan_eyes = isnan(temp_eye_sparsed);
        segments_to_interpret =  [];
        very_short_bits = findgaps(find(nan_eyes == 1));
        for vbs = 1:size(very_short_bits,1)
            these_vbs = very_short_bits(vbs,:);
            these_vbs(these_vbs == 0) = [];
            if length(these_vbs) <= minNanTime
                segments_to_interpret = [segments_to_interpret these_vbs];
            end
        end
        vps = interp1(find(~isnan(temp_eye_sparsed)),temp_eye_sparsed(~isnan(temp_eye_sparsed)),segments_to_interpret,'pchip');
        
        temp_eye_interpolated = temp_eye_sparsed;
        temp_eye_interpolated(segments_to_interpret) = vps;
        
        %---#3: Remove data that is too short---%
        [temp_eye_shortened,~] = remove_short_segements(temp_eye_interpolated,time_too_short);
        
        if all(isnan(temp_eye_shortened))
            cleaned_gaze_data{lr,xyp} = temp_eye_shortened;
            continue
        end
        
        %---#4 Remove edges of good data which can be artifacts----%
        temp_eye_ended = temp_eye_shortened;
        [~,strtgood,ndgood] = findgaps(find(~isnan(temp_eye_shortened)));
        for stg = 1:length(strtgood)
            this_gaze_segement = temp_eye_shortened(strtgood(stg):ndgood(stg));
            medfilt_seg = medfilt1(this_gaze_segement,round(time_too_short/4),'truncate');
            raw_filt_diff = this_gaze_segement-medfilt_seg;
            end_thresh = lambda*mad(raw_filt_diff);
            [~,start_blank,end_blank] = findgaps(find(abs(raw_filt_diff) > end_thresh));
            if any(abs(raw_filt_diff(1:blank_samples)) >= end_thresh)
                these_start_ind = find(start_blank <= blank_samples);
                if isempty(these_start_ind)
                    error('How is this empty')
                end
                if end_blank(these_start_ind) > 4*blank_samples
                    error('thats a lot of blanking to do...so doing less')
                    temp_eye_ended(strtgood(stg):strtgood(stg)+blank_samples-1) = NaN;
                else
                    temp_eye_ended(strtgood(stg):strtgood(stg)+end_blank(these_start_ind)-1) = NaN;
                end
            end
            if any(abs(raw_filt_diff(end-blank_samples+1:end)) >= end_thresh)
                these_end_ind = find(end_blank >= length(this_gaze_segement)-blank_samples);
                if isempty(these_end_ind)
                    error('How is this empty')
                end
                
                if length(this_gaze_segement)-start_blank(these_end_ind) > 4*blank_samples
                    error('thats a lot of blanking to do')
                end
                temp_eye_ended(strtgood(stg)+start_blank(these_end_ind)+1:ndgood(stg)) = NaN;
            end
        end
        
        %---#5 Remove Single Sample Spikes with Median filter---%
        temp_med_filt = medfilt1(temp_eye_ended,median_spike_filter_width,'truncate');
        
        %---#6 De-Noise---%
        %basically find points that are substantially (lambda) different more than
        %would be expected if over smooth, then replace cruddy points and
        %neighboring points with over smoothed data
        temp_denoised = temp_eye_ended;
        [~,startgood,endgood] = findgaps(find(~isnan(temp_eye_ended)));
        for st = 1:length(startgood)
            this_good_segment = temp_eye_ended(startgood(st):endgood(st));
            temp_noise_filt = medfilt1(this_good_segment,floor(disparity_medial_filter_width),'truncate');
            raw_filt_diff_noise = this_good_segment-temp_noise_filt;
            filt_noise_diff_thresh = lambda*mad(raw_filt_diff_noise);
            above_thresh = find(abs(raw_filt_diff_noise) > filt_noise_diff_thresh); %start bits ignore
            above_thresh(above_thresh <= blank_samples) = [];
            above_thresh(above_thresh >= length(this_good_segment)-blank_samples) = [];%end bits so ignore
            denoised_segment = this_good_segment;
            for at = 1:length(above_thresh)
                denoised_segment(above_thresh(at)-1:above_thresh(at)+1) = temp_noise_filt(above_thresh(at)-1:above_thresh(at)+1);
            end
            temp_denoised(startgood(st):endgood(st)) = denoised_segment;
        end
        
        cleaned_gaze_data{lr,xyp} = temp_denoised;
        
        if plot_results
            %%
            figure
            plot(temp_eye)
            hold on
            plot(temp_eye_sparsed)
            plot(temp_eye_interpolated)
            plot(segments_to_interpret,vps,'r*')
            plot(temp_eye_shortened)
            plot(temp_eye_ended)
            plot(temp_med_filt)
            plot(cleaned_gaze_data{lr,xyp})
            hold off
            box off
            xlabel('Sample #')
            ylabel('Eye Position (a.u.)')
            legend('Raw','Sparsed','Interpolated','Missing Points','Shortened','Ended','Spiked','DeNoised')
            %%
        end
    end
end

%%
%%%%%%%%%%%%%%%%%%%%%%%%%
%---Combined L/R Eyes---%
%%%%%%%%%%%%%%%%%%%%%%%%%
for xyp = 1:3
    
    disparity = cleaned_gaze_data{1,xyp} - cleaned_gaze_data{2,xyp};
    disparity = medfilt1(disparity,disparity_medial_filter_width,'truncate');%remove random points that may have more disparity than expected
    avg_disparity = nanmean(disparity);
    
    if xyp == 3 %pupil
        %pupil units unkown so calculate equivalent threshold for pupil data
        disparity_thresh = (nanmean(cleaned_gaze_data{1,xyp}) + nanmean(cleaned_gaze_data{2,xyp}))/2 * gaze_disparity_thresh;
    else
        disparity_thresh = gaze_disparity_thresh;
    end
    
    discard_points = zeros(1,length(disparity));
    discard_points(abs(disparity) > disparity_thresh) = 1;
    
    if sum(discard_points)/sum(~isnan(disparity)) > 0.33
        %%
        discard_ind = find(discard_points == 1);
        figure
        plot(cleaned_gaze_data{1,xyp});
        hold on
        plot(cleaned_gaze_data{2,xyp})
        plot(discard_ind,cleaned_gaze_data{1,xyp}(discard_ind));
        plot(discard_ind,cleaned_gaze_data{2,xyp}(discard_ind));
        hold off
        
        %str = input('Do you wish to accept the removal of high disparity data from this trial (Y/N)?','s');
        if 1%strcmpi(str,'Y')
            close
        else
            error('Lots of disparity')
        end
        
    end
    
    true_disparity = disparity;
    true_disparity(discard_points == 1) = NaN;
    
    %Remove Segments that are too short, so we don't reintroduce them
    [true_disparity,removed_short_true_disparity] = remove_short_segements(true_disparity,time_too_short/2);
    discard_points(removed_short_true_disparity == 1) = 1;
    true_avg_disparity = nanmean(disparity);
    
    if isnan(true_avg_disparity)
        %means only have 1 eye the entire time so there is no disparity
        true_avg_disparity = 0;
    end
    
    left = cleaned_gaze_data{1,xyp};
    left(discard_points == 1) = NaN;
    [left,~] = remove_short_segements(left,time_too_short);
    
    right = cleaned_gaze_data{2,xyp};
    right(discard_points == 1) = NaN;
    [right,~] = remove_short_segements(right,time_too_short);
    
    combined_corrected = NaN(1,length(left));
    oneEye = find(isnan(left) ~= isnan(right));
    bothEyes = ~isnan(left) & ~isnan(right);
    combined_corrected(bothEyes) = nanmean([left(bothEyes),right(bothEyes)], 2);
    
    %for segments with only 1 eye find the closes disparity measure to
    %fix instead of using the average of the whole session...should
    %create smoother transition
    [~,startInd,endInd] = findgaps(oneEye);
    for ist =1:length(startInd)
        st = startInd(ist);
        fn = endInd(ist);
        
        if ist == 1
            start_ind = 1;
        else
            start_ind = endInd(ist-1)+1;
        end
        if ist == length(startInd)
            end_ind = length(left);
        else
            end_ind = startInd(ist+1)-1;
        end
        
        if fn + disparity_window > length(true_disparity)
            disparity_window2 =  length(true_disparity)-fn;
        else
            disparity_window2 = disparity_window;
        end
        
        if all(isnan(true_disparity(start_ind:st-1)))%everything before is NaN
            if all(isnan(true_disparity(fn+1:fn+disparity_window2)))%everything after is NaNs too
                correcting_offset = true_avg_disparity*ones(fn-st+1,1);
            else
                correcting_offset = nanmean(true_disparity(fn+1:fn+disparity_window2))*ones(fn-st+1,1);
            end
        elseif all(isnan(true_disparity(fn+1:end_ind)))%everything after is NaNs
            if all(isnan(true_disparity(st-disparity_window2:st-1)))%everthing before is NaNs
                correcting_offset = true_avg_disparity*ones(fn-st+1,1);
            else
                correcting_offset = nanmean(true_disparity(st-disparity_window2:st-1))*ones(fn-st+1,1);
            end
        else
            prior_offset = nanmean(true_disparity(st-disparity_window:st-1));
            post_offset = nanmean(true_disparity(fn+1:fn+disparity_window2));
            
            %try sloping offset change generally not going to affect
            %anything in the gaze data if slow enough
            P = polyfit([1 fn-st+1],[prior_offset post_offset],1);
            correcting_offset = polyval(P,1:fn-st+1)';
        end
        
        
        if all(isnan(left(st:fn)))
            combined_corrected(st:fn) = right(st:fn) +  correcting_offset/2;
        elseif all(isnan(right(st:fn)))
            combined_corrected(st:fn) = left(st:fn) -  correcting_offset/2;
        else
            left_nan = find(isnan(left(st:fn)));
            right_nan = find(isnan(right(st:fn)));
            [Cinter,~,~] = intersect(left_nan,right_nan);
            if isempty(Cinter)%means one ends right as the other starts
                %double check this is true
                if ~(left_nan(1) == right_nan(end)+1 || left_nan(end)+1 == right_nan(1))
                    error('LEft and right dont end and start at same point!')
                end
                %do it the simple way
                combined_corrected(st:fn) = right(st:fn) +  correcting_offset/2;
                combined_corrected(st:fn) = left(st:fn) -  correcting_offset/2;
            else
                error('Why are there no NaNs')
            end
        end
        
    end
    
    if (any(combined_corrected > 1.01*xy_max_thresh) || any(combined_corrected < 1.01*xy_min_thresh)) && xyp ~= 3
        error('Why are XY values greater than thresholds?')
    end
    
    if xyp == 1
        Combined_X = combined_corrected;
    elseif xyp == 2
        Combined_Y = combined_corrected;
    else
        Combined_Pupil = combined_corrected;
    end
    
    if plot_results
        figure
        plot(cleaned_gaze_data{1,xyp})
        hold on
        plot(cleaned_gaze_data{2,xyp})
        plot(combined_corrected)
        hold off
        legend('Left','Right','Combined')
        xlabel('Sample #')
        ylabel('Eye Position (a.u)')
        box off
    end
end
end


function [shortened_signal,remove_signal] = remove_short_segements(input_signal,min_length)
%remove segments of Non-Nans shorter than min_length
remove_signal = zeros(size(input_signal));
[~,strt,fnsh] = findgaps(find(~isnan(input_signal)));
if ~isempty(strt)
    for ind = 1:length(strt)
        if fnsh(ind)-strt(ind) < min_length
            remove_signal(strt(ind):fnsh(ind)) = 1;
        end
    end
    input_signal(remove_signal == 1) = NaN;
    shortened_signal = input_signal;
else
    shortened_signal = input_signal;
end
end