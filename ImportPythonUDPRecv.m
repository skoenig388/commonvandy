function datatbl = ImportPythonUDPRecv(filename,import_opts)
%imports Python UDP Recv data
%Written by Seth Konig 2/15/19

try
    import_opts.SelectedVariableNames = {'Var4','Var6'};
    datatbl = readtable(filename,import_opts);
    datatbl.Properties.VariableNames = {'UnitySentFrame','PythonTimeStamp'};
catch
    disp(['Could not properly import ' filename]);
    datatbl = [];
end

end