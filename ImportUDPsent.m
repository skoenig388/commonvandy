function datatbl = ImportUDPsent(filename,import_opts)

%imports Python UDP Sent data
%Written by Seth Konig 2/15/19

try
    import_opts.SelectedVariableNames = {'Var2','Var3','Var4'};
    datatbl = readtable(filename,import_opts);
    datatbl.Properties.VariableNames = {'Frame','UnityTime','Message'};
catch
    disp(['Could not properly import ' filename]);
    datatbl = [];
end