function lp = FindLp(outcome, varargin)
%can institute any Lp definitions we want, defined by first string in
%varargin

%outcome must be a column vector!

%outcome is accuracy per trial

%currently only 1 lp definition
%looks for first correct trial where forward-looking sliding window acc is
%greater than some threshold.
%varargin: 1 = 'slidingwindow', 2 = window size (integer), 3 = threshold
%(proportion correct)

if strcmpi(varargin{1}, 'slidingwindow')
    accForward = SlidingWindowForward(outcome,varargin{2});
    lp = find(accForward >= varargin{3} & outcome == 1 & (1:length(outcome))' < (length(outcome) - varargin{2} + 1),1);
elseif strcmpi(varargin{1}, 'futureacc')
    accForward = SlidingWindowForward(outcome,length(outcome));
    lp = find(outcome==1 & accForward > varargin{2},1);
    if length(outcome) - lp < 5
        lp = NaN;
    end
elseif strcmpi(varargin{1}, 'em')
    M.Responses = outcome;
    M = get_learningCurveStat_01(M);
    lp = find(M.p05>0.5,1);
end

if isempty(lp)
    lp = NaN;
end