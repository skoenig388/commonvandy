function parsedData = ParseNeurarduinoData2(rawData)
% parsedData = ParseNeurarduinoData2(rawData)
%
% rawData is a table with 4 columns; Subject, FrameReceieved, FrameStart,
% and Message

%% preproc and settings
commandStrings = {'HLP', 'ECH', 'IDQ', 'INI', 'LOG', 'LIN', 'LVB', 'TPW', 'TPP', 'TIM', 'RWD', ...
    'NSU', 'NHD', 'NPD', 'NEU', 'NDW', 'CSL', 'CSR', 'CSI', 'CAO', 'CAF', 'CTR', 'CTL', 'FIL', 'FST', 'FSW', 'XAL', 'XJL'};

processedData = rawData;
processedData.FrameStart = processedData.FrameStart * 1000;
vname = rawData.Properties.VariableNames;
Unknown = array2table(zeros(0,numel(vname)),'VariableNames',vname);

%% get data about the system state
%disp('compiling system reports...')
%BV: ugly and unsafe...

% start samples: 'System state (all values in base 10):'
st = find(strncmp(processedData.Message,'S',1));
del = false(size(st));
for ii=1:numel(st)
    m = processedData.Message{st(ii)};
    if numel(m) > 2
        if ~strcmp(m(1:3), 'Sys')
            del(ii) = 1;
        end
    else %check if adding the next line makes a difference
        m = [m, processedData.Message{st(ii)+1}];
        if ~strcmp(m(1:3), 'Sys')
            del(ii) = 1;
        end
    end
end
st(del) = [];

% end samples: 'End of system state.'
fn = find(contains(processedData.Message,'.')); 
del = false(size(fn));
for ii=1:numel(fn)
    m = processedData.Message{fn(ii)};
    if numel(m) > 2
        if ~strcmp(m(end-2:end), 'te.')
            del(ii) = 1;
        end
    else %check if adding the next line makes a difference
        m = [m, processedData.Message{fn(ii)+1}];
        if ~strcmp(m(end-2:end), 'te.')
            del(ii) = 1;
        end
    end
end
fn(del) = [];

%compile
SystemStatus = [];
del = false(size(processedData,1),1);
for ii=1:numel(st)
    ind = st(ii):fn(ii);
    tmp = processedData(ind,:);
    
    if ii==1; SystemStatus = tmp;
    else; SystemStatus = cat(1,SystemStatus,tmp);
    end
    
    del(ind) = 1;
end
processedData(del,:) = [];

%% clean the data
%disp('cleaning/concatenating split messages...')

cases = {  'T',23; %photodiode
            'S',37; %system, or sync
%             'Sys',37; % start system report
%             'Syn',15; %sync
%             'E',20; %end system report
            'R',21; %reward
            'L',19  %neuralynx
        };

for ic=1:size(cases,1)
    
    c = cases{ic,1};
    
    %disp(['...case ' c])
    flag = true;
    while flag
        % right type, right size, make sure its not a command...
        bad = strncmp(processedData.Message,c,numel(c)) &...
            cellfun(@numel,processedData.Message) < cases{ic,2} & ...
            ~cellfun(@(x) ismember(x,commandStrings),processedData.Message);
            
        bad = find(bad);
        
        if numel(bad) == 0
            flag = 0;
        else
            % if the truncated message is at the end, we probbaly manually
            % ended the experiment
            if bad(end)==numel(processedData.Message) 
                processedData(end,:) = [];
            else
                % try concatenating next message...
                tmp_message = cellfun(@(x,y) [x y],processedData.Message(bad),processedData.Message(bad+1),'uniformoutput',0);
                
                tooLong = cellfun(@numel,tmp_message) > cases{ic,2};
                tooLong = bad(tooLong);
                
                if ~isempty(tooLong) % if curren message is too long now, this message i garbage...
                    Unknown = cat(1,Unknown,processedData(tooLong,:));
                    processedData(tooLong,:) = [];
                else
                    %some manual checks...
                    switch c
                        case 'L'
                            %soemtimes, space get deleted, reinsert it if its
                            %missing
                            s = [6 15]; %expected space
                            messagesThatNeedSpace = find( cellfun(@(x) numel(x) >= cases{ic,2} - 2 & numel(x) < cases{ic,2},tmp_message) );
                            indexMissingSpaces = cellfun(@(x) s(~ismember(s,strfind(x,' '))), tmp_message(messagesThatNeedSpace), 'uniformoutput',0);
                            tmp_message(messagesThatNeedSpace) = cellfun(@(x,y) insertAfter(x,y-1,' '),tmp_message(messagesThatNeedSpace),indexMissingSpaces, 'uniformoutput',0);
                    end

                    % update message
                    processedData.Message(bad) = tmp_message;            
                    processedData(bad+1,:) = [];
                end
            end
        end
        foo = 1;
    end
end


%% parse each message type

%disp('parsing...')
processedMessage = false(size(processedData.Message));

% sync details
%disp('...sync details')
isync = strncmp(processedData.Message,'Syn',3) & ~processedMessage;
processedMessage(isync) = 1;
SyncDetails = processedData(isync,:);
if sum(isync)~=0
    SyncDetails.ArduinoTimestamp = hex2dec( cellfun(@(x) x(8:15),SyncDetails.Message,'uniformoutput',0)) / 10;
    SyncDetails = removevars(SyncDetails,'Message');
end

% standard neurarduino joystick + photo data line (every 3.3 ms)
%disp('...joy and photo details')
ijoy = find( strncmp(processedData.Message,'T',1) & ~processedMessage );
processedMessage(ijoy) = 1;
joyAndPhotoDetails = processedData(ijoy,:);
if sum(ijoy)~=0
    m = joyAndPhotoDetails.Message;

    joyAndPhotoDetails.ArduinoTimestamp = hex2dec(cellfun(@(x) x(2:9),m,'uniformoutput',0))/10;

    joyAndPhotoDetails.JoyX = hex2dec( cellfun(@(x) x(11:12),m, 'uniformoutput',0) );
    joyAndPhotoDetails.JoyY = hex2dec( cellfun(@(x) x(13:14),m, 'uniformoutput',0) );
    joyAndPhotoDetails.JoyZ = hex2dec( cellfun(@(x) x(15:16),m, 'uniformoutput',0) );
    
    joyAndPhotoDetails.PhotoL = hex2dec( cellfun(@(x) x(18:19),m, 'uniformoutput',0) );
    joyAndPhotoDetails.PhotoR = hex2dec( cellfun(@(x) x(20:21),m, 'uniformoutput',0) );

    joyAndPhotoDetails.NA_PhotoLStatus = nan(size(joyAndPhotoDetails,1),1);
    joyAndPhotoDetails.NA_PhotoLStatus( cellfun(@(x) strcmp(x(22),'B'),m) ) = 0;
    joyAndPhotoDetails.NA_PhotoLStatus( cellfun(@(x) strcmp(x(22),'W'),m) ) = 1;
    
    joyAndPhotoDetails.NA_PhotoRStatus = nan(size(joyAndPhotoDetails,1),1);
    joyAndPhotoDetails.NA_PhotoRStatus( cellfun(@(x) strcmp(x(23),'B'),m) ) = 0;
    joyAndPhotoDetails.NA_PhotoRStatus( cellfun(@(x) strcmp(x(23),'W'),m) ) = 1;
    
    joyAndPhotoDetails = removevars(joyAndPhotoDetails,'Message');
end


% reward details
%disp('...reward')
irew = strncmp(processedData.Message,'R',1) & ~processedMessage;
processedMessage(irew) = 1;
RewardDetails = processedData(irew,:);
if sum(irew)~=0
    RewardDetails.ArduinoTimestamp = hex2dec( cellfun(@(x) x(9:16),RewardDetails.Message, 'uniformoutput',0) ) / 10;
    RewardDetails.RewardDuration = hex2dec( cellfun(@(x) x(18:21),RewardDetails.Message, 'uniformoutput',0) );
    RewardDetails = removevars(RewardDetails,'Message');
end 

 %event code ("lynx")
 %disp('...event codes')
icode = strncmp(processedData.Message,'L',1) & ~processedMessage;
processedMessage(icode) = 1;
EventCodeDetails = processedData(icode,:);
if sum(icode)~=0
    EventCodeDetails.ArduinoTimestamp = hex2dec( cellfun(@(x) x(7:14), EventCodeDetails.Message, 'uniformoutput',0) ) / 10;
    EventCodeDetails.EventCode = hex2dec( cellfun(@(x) x(16:19),EventCodeDetails.Message,'uniformoutput',0) );
    EventCodeDetails = removevars(EventCodeDetails,'Message');
end 

%find empty strings that were not precess
%%disp('....empty strings')
empt = find(cellfun(@(x) isempty(x),processedData.Message) == 1);
processedMessage(empt) = 1;
%disp(['Found ' num2str(length(empt)) ' empty messages'])
for e = 1:length(empt)
    processedData.Message{empt(e)} = 'ReplacedEmpty';  
end

%find strings that were cut off part way through, added SDK 2/11/19
%%disp('....empty strings')
empt = find(cellfun(@(x) length(x) <= 2,processedData.Message) == 1);
processedMessage(empt) = 1;
%disp(['Found ' num2str(length(empt)) ' empty messages'])
for e = 1:length(empt)
    processedData.Message{empt(e)} = 'ReplacedCutoff';  
end

%command strings
%%disp('...command strings')
icmd = cellfun(@(x) ismember(x(1:3),commandStrings),processedData.Message) & ~processedMessage;
Commands = processedData(icmd,:);
 
 % all other messages
%disp('...unknown')
Unknown = cat(1,Unknown,processedData(~processedMessage,:));


%% update final
parsedData.JoyAndPhotoDetails = joyAndPhotoDetails;
parsedData.SyncDetails = SyncDetails;
parsedData.RewardDetails = RewardDetails;
parsedData.EventCodeDetails = EventCodeDetails;
parsedData.Commands = Commands;
parsedData.SystemStatus = SystemStatus;
parsedData.Unknown = Unknown;

% to be consistent with old script
structFields = fields(parsedData);
for ii = 1:length(structFields)
    if isempty(parsedData.(structFields{ii}))
        parsedData.(structFields{ii}) = NaN;
    end
end

foo=1;
