function [stimulus_path] = Determine_Quaddle_Path(stim_name,root_path,checkexistence)

if nargin < 2
    root_path = 'D:\\QuaddleRepository_20180515\\';
    checkexistence = true;
elseif nargin < 3
   checkexistence = true; 
end

shape_stim_path = [root_path 'Only_Shape\\'];
arm_stim_path = [root_path 'Only_Arm\\'];
pattern_stim_path = [root_path 'Only_Pattern\\'];
texture_stim_path = [root_path 'Only_Texture\\'];
color_stim_path = [root_path 'Only_Color\\'];
grayscale_pattern_path = [root_path 'Patterned_GrayScale\\'];


patternless_stim_path = [root_path 'Patternless_'];

%first parse name
shape_value = str2double(stim_name(strfind(stim_name,'S')+1:strfind(stim_name,'S')+2));
pattern_value = str2double(stim_name(strfind(stim_name,'P')+1:strfind(stim_name,'P')+2));
texture_value = str2double(stim_name(strfind(stim_name,'T')+1:strfind(stim_name,'T')+2));

color_value1 = str2double(stim_name(strfind(stim_name,'C')+1:strfind(stim_name,'C')+7));
color_value2 = str2double(stim_name(strfind(stim_name,'C')+9:strfind(stim_name,'C')+15));

arm_angle_value = str2double(stim_name(strfind(stim_name,'A')+1:strfind(stim_name,'A')+2));
arm_end_values = str2double(stim_name(strfind(stim_name,'E')+1:strfind(stim_name,'E')+2));

if shape_value == 0 && pattern_value == 0 && texture_value == 0 && arm_angle_value == 0 && arm_end_values == 0 &&...
        (color_value1 == 5000000 || color_value1 == 6000000 || color_value1 == 7000000)
    %then it is a neutral Quaddle
    stimulus_path = root_path;
elseif color_value1 == 7000000 && color_value2 == 5000000 && pattern_value ~= 0
    stimulus_path = grayscale_pattern_path;
elseif shape_value == 0 && pattern_value == 0 && texture_value == 0 && arm_angle_value == 0 && arm_end_values == 0  && ...
        (color_value1 ~= 5000000 || color_value1 ~= 6000000 || color_value1 ~= 7000000)
    stimulus_path = color_stim_path;
elseif pattern_value == 0  && color_value1 >= 7000000
    stimulus_path = [patternless_stim_path '70\\'];
elseif pattern_value == 0 && color_value1 >= 6000000
    stimulus_path = [patternless_stim_path '60\\'];
elseif pattern_value == 0 && texture_value == 0 && arm_angle_value == 0 && arm_end_values == 0 && shape_value ~= 0 &&...
        (color_value1 == 5000000 || color_value1 == 6000000 || color_value1 == 7000000)
    stimulus_path = shape_stim_path;
elseif pattern_value ~= 0 && texture_value == 0 && arm_angle_value == 0 && arm_end_values == 0 && shape_value == 0 &&...
        (color_value1 == 5000000 || color_value1 == 6000000 || color_value1 == 7000000)
    stimulus_path = pattern_stim_path;
elseif pattern_value == 0 && texture_value ~= 0 && arm_angle_value == 0 && arm_end_values == 0 && shape_value == 0 &&...
        (color_value1 == 5000000 || color_value1 == 6000000 || color_value1 == 7000000)
    stimulus_path = texture_stim_path;
elseif pattern_value == 0 && texture_value == 0 && (arm_angle_value ~= 0 || arm_end_values ~= 0) && shape_value == 0 &&...
        (color_value1 == 5000000 || color_value1 == 6000000 || color_value1 == 7000000)
    stimulus_path = arm_stim_path;
else
    stimulus_path = [root_path 'Patterned_S0' num2str(shape_value) '\\'];
end

if checkexistence
    if ~exist([stimulus_path stim_name],'file')
        error('Quaddle Does not Exist in Current Context')
    end
end
end

