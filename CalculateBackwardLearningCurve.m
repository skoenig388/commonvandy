function [observed_backward_learning_curves,ground_truth_backward_learning_curves] = ...
    CalculateBackwardLearningCurve(learning_curve,observed_LPs,groud_truth_LPs,...
    trials_in_block,forward_back_trials)
%Coped from elsewhere by Seth Koenig 6/4/19

num_curves = size(learning_curve,1);
observed_backward_learning_curves = NaN(num_curves,2*forward_back_trials+1);
ground_truth_backward_learning_curves = NaN(num_curves,2*forward_back_trials+1);

for iter = 1:num_curves
    lp = observed_LPs(iter);
    if ~isempty(lp) && ~isnan(lp) && lp ~= trials_in_block
        if lp <= forward_back_trials
            start_index = forward_back_trials-lp+1;
            observed_backward_learning_curves(iter,start_index+1:end) = learning_curve(iter,1:lp+forward_back_trials);
        elseif lp >= trials_in_block-forward_back_trials
            end_index = (lp+forward_back_trials)-trials_in_block;
            observed_backward_learning_curves(iter,1:end-end_index) = learning_curve(iter,lp-forward_back_trials:end);
        else
            observed_backward_learning_curves(iter,:) = learning_curve(iter,lp-forward_back_trials:lp+forward_back_trials);
        end
    end
    
    lp = groud_truth_LPs(iter);
    if ~isempty(lp) && ~isnan(lp) && lp ~= trials_in_block
        if lp <= forward_back_trials
            start_index = forward_back_trials-lp+1;
            ground_truth_backward_learning_curves(iter,start_index+1:end) = learning_curve(iter,1:lp+forward_back_trials);
        elseif lp >= trials_in_block-forward_back_trials
            end_index = (lp+forward_back_trials)-trials_in_block;
            ground_truth_backward_learning_curves(iter,1:end-end_index) = learning_curve(iter,lp-forward_back_trials:end);
        else
            ground_truth_backward_learning_curves(iter,:) = learning_curve(iter,lp-forward_back_trials:lp+forward_back_trials);
        end
    end
end

observed_backward_learning_curves = nanmean(observed_backward_learning_curves);
ground_truth_backward_learning_curves = nanmean(ground_truth_backward_learning_curves);

end