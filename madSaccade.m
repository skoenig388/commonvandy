function sacInd = madSaccade(xdeg,ydeg,fsample,trialIndices)
% sacInd = madSaccade(xdeg,ydeg,fsample)
% sacInd = madSaccade(xdeg,ydeg,fsample,trialIndices)

%% settings and checks
plotFig = 0;

%fsample = 300;
wind = fliplr( (fsample/6)*[-1 -1 0 1 1] ); %Engbert et al 2006, 2003
sg_len = ceil(0.02*fsample);

nsmp = numel(xdeg);
lambdaAcc = 6;
errDeg = 1;
useMadAlgo = 1;
tMinInterSac = ceil(0.02*fsample);
durationThresh = ceil(0.01*fsample);

if nargin < 4 || isempty(trialIndices)
    trialIndices = [1, numel(xdeg)];
end

sacInd = []; %start the output here in case we return early

%% do smoothing to get velocity
N = 2;                 % Order of polynomial fit
F = 2*ceil(sg_len)-1;    % Window length
[b,g] = sgolay(N,F);   % Calculate S-G coefficients
Nf = F;

xdeg2 = conv(xdeg, g(:,1)', 'same');
xdeg2(1:(Nf-1)/2) = xdeg(1:(Nf-1)/2);
xdeg2(end-(Nf-3)/2:end) = xdeg(end-(Nf-3)/2:end);
ydeg2 = conv(ydeg, g(:,1)', 'same');
ydeg2(1:(Nf-1)/2) = ydeg(1:(Nf-1)/2);
ydeg2(end-(Nf-3)/2:end) = ydeg(end-(Nf-3)/2:end);

Vel(:,1) = conv(xdeg, -g(:,2)', 'same');
Vel(1:(Nf-1)/2,1) = xdeg(1:(Nf-1)/2);
Vel(end-(Nf-3)/2:end,1) = xdeg(end-(Nf-3)/2:end);
Vel(:,2) = conv(ydeg, -g(:,2)', 'same');
Vel(1:(Nf-1)/2,2) = ydeg(1:(Nf-1)/2);
Vel(end-(Nf-3)/2:end,2) = ydeg(end-(Nf-3)/2:end);
Vel = Vel * fsample;


%% find threshold

% get angular acceleration
V = abs(complex(Vel(:,1),Vel(:,2)));
A = conv(V,wind,'same');

AccThresh = nan(nsmp,2);
for it=1:size(trialIndices,1)
    %disp(it)
    st = trialIndices(it,1);
    fn = trialIndices(it,2);
    tmpa = A(st:fn);
    
    th = iterativeThreshold(tmpa,[],errDeg,lambdaAcc,[],useMadAlgo);
    AccThresh(st:fn,1) = th;
    AccThresh(st:fn,2) = -th;
end

%% detect saccades
%look for increases and decreases in acceleration as the offset and
%onsets
ainc = A > AccThresh(:,1);
adec = A < AccThresh(:,2);

% estimated start and end are moments of peak acceleration (but could be
% first and last sample of detection, for example...)
startEst = [];
endEst = [];
[st,fn] = find_borders(ainc);
for ii=1:numel(st)
    [~,startEst(ii)] = max(A(st(ii):fn(ii)));
    startEst(ii) = startEst(ii)  + st(ii) - 1;
end
[st,fn] = find_borders(adec);
for ii=1:numel(st)
    [~,endEst(ii)] = min(A(st(ii):fn(ii)));
    endEst(ii) = endEst(ii)  + st(ii) - 1;
end

% [startEst,~] = find_borders(ainc); %alterantive, first and last sample of detection is estimated start and end of saccade
% [~,endEst] = find_borders(adec);

%now match
SacStart = [];
SacEnd = [];
ii = 0;
for isac=1:numel(startEst)
    st = startEst(isac);
    fn = endEst( find(endEst>st,1) );
    
    %makre sure that the proposed end is before another proposed start
    if ~isempty(fn) && (isac==numel(startEst) || fn < startEst(isac+1))
        ii = ii+1;
        SacStart(ii,1) = st;
        SacEnd(ii,1) = fn;
    end
end

% delete saccades at edges
if ~isempty(SacStart)
    if SacStart(1)==1
        SacStart(1) = [];
        SacEnd(1) = [];
    end
    if SacEnd(end)==nsmp
        SacStart(end) = [];
        SacEnd(end) = [];
    end
end

%combine saccades that are too close together
d1 = diff([SacEnd(1:end-1), SacStart(2:end)],[],2);
tooClose = find(d1 < tMinInterSac);
SacStart(tooClose+1) = [];
SacEnd(tooClose) = [];
if isempty(SacStart) || isempty(SacEnd); return; end

%delete Saccades that are too short
d2 = diff([SacStart, SacEnd],[],2);
tooShort = find( d2 <= durationThresh );
SacStart(tooShort) = [];
SacEnd(tooShort) = [];
if isempty(SacStart) || isempty(SacEnd); return; end

%% output
sacInd = false(nsmp,1);
for ii=1:numel(SacStart)
    st = SacStart(ii);
    fn = SacEnd(ii);
    sacInd(st:fn) = 1;
end

%% plots
if plotFig
    
    %prepare
    selind = 1:nsmp;
    %selind = (1:1000) + 150;
    t = linspace(1,nsmp/fsample,nsmp); %make time axis
    t = t(selind) - t(selind(1));
    
    sac = nan(size(sacInd));
    sac(sacInd) = 1;
    sac = sac(selind);
    
    %plot
    figure('position',get(0,'screensize'))
    nr = 3; nc = 1;
    wd = 2;
    
    %plot positions
    subplot(nr,nc,1)
    pp = plotyy(t,xdeg(selind),t,ydeg(selind));
    hold all
    y = get(gca,'ylim');
    y = (y(2) - 0.05*abs(y(2))) * sac;
    plot(t,y,'k','linewidth',wd)
    title('position')
    ylabel(pp(1),'x-pos (deg)')
    ylabel(pp(2),'y-pos (deg)')
    
    %velocity
    subplot(nr,nc,2)
    v =V(selind);
    plot(t,v)
    hold all
    y = max(v);
    y = (y + 0.01*abs(y)) * sac;
    plot(t,y,'k','linewidth',wd)
    title('Velocity')
    ylabel('Vel (deg/s)')
    set(gca,'ylim',[0, max(v) + abs(max(v))*0.05])
    
    %acceleration
    subplot(nr,nc,3)
    a = A(selind);
    plot(t,a)
    hold all
    y = max(a);
    y = (y + 0.01*abs(y)) * sac;
    plot(t,y,'k','linewidth',wd)
    title('Acceleration')
    ylabel('Acc (deg/s)')
    set(gca,'ylim',[min(a) - abs(min(a))*0.05, max(a) + abs(max(a))*0.05])
end

foo=1;

end
%% ------------------------------------------------------------------------
% MISC

function [startEst, endEst] = find_borders(borders)
% [startEst, endEst] = find_borders(borders)

%checks
if islogical(borders)
    borders = diff(borders);
else
    if any(~ismember(borders,[-1 0 1]))
        error('borders must either be a logical vector, or a difference of a logical vector')
    end
end

if isrow(borders); doTranspose = 1; borders = borders';
else doTranspose = 0;
end

%estimate borders
startEst = find(borders==1)+1;
endEst = find(borders==-1);

if isempty(startEst) || isempty(endEst)
    return
end


%if the start/end of the data has bad data, then we dont have an estimate
%for the start/end of the blink. blink starts/ends at start/end of recording
if endEst(1) < startEst(1)
    %disturbance_all{3}(endEst(1)) = 1;
    startEst = [1;startEst];
end

if endEst(end) < startEst(end)
    %disturbance_all{3}(endEst(end)) = 1;
    endEst = [endEst;numel(borders)];
end

%transpose to match input
if doTranspose
    startEst = startEst';
    endEst = endEst';
end

startEst(startEst > numel(borders)) = numel(borders);
end