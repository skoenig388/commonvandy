function ExptParamsData = ReadExperimentalParameters(filename)

%---Use Low Level File I/O to read in data---%
fid = fopen(filename);
exptparamstext = textscan(fid,'%s');
fclose(fid);



%----Turn exptparamstext data into struepture----%
exptparamstext = exptparamstext{1};

relevantobjectpath = find(contains(exptparamstext,'RelevantObjectPath'));
irrelevantobjectpath = find(contains(exptparamstext,'IrrelevantObjectPath'));
stim_extensions = find(contains(exptparamstext,'StimExtension'));
if stim_extensions- irrelevantobjectpath == 3
    %file path for irrelvant objects split in 2
    exptparamstext{irrelevantobjectpath+1} = [exptparamstext{irrelevantobjectpath+1} exptparamstext{irrelevantobjectpath+2}];
    exptparamstext(irrelevantobjectpath+2) = [];
elseif  stim_extensions- irrelevantobjectpath > 3
    error('Irrelevant Path split more than expected');
end
if irrelevantobjectpath-relevantobjectpath == 3
    exptparamstext{relevantobjectpath+1} = [exptparamstext{relevantobjectpath+1} exptparamstext{relevantobjectpath+2}];
    exptparamstext(relevantobjectpath+2) = [];
elseif irrelevantobjectpath-relevantobjectpath > 3
    error('Relevant Path split more than expected');
end
exptparamstext = cellfun(@(s) erase(s,'"'), exptparamstext, 'UniformOutput', false);
exptparamstext = cellfun(@(s) erase(s,':'), exptparamstext, 'UniformOutput', false);
exptparamstext = cellfun(@(s) erase(s,'...'), exptparamstext, 'UniformOutput', false);
exptparamstext = cellfun(@(s) erase(s,'{'), exptparamstext, 'UniformOutput', false);
exptparamstext = cellfun(@(s) erase(s,'}'), exptparamstext, 'UniformOutput', false);
exptparamstext = cellfun(@(s) erase(s,']'), exptparamstext, 'UniformOutput', false);
exptparamstext = cellfun(@(s) erase(s,'['), exptparamstext, 'UniformOutput', false);
exptparamstext = cellfun(@(s) erase(s,'{'), exptparamstext, 'UniformOutput', false);

empties = cellfun(@isempty, exptparamstext);
exptparamstext(empties) = [];
exptparamstext = cellfun(@(s) strtrim(s), exptparamstext, 'UniformOutput', false);

feature_names = find(contains(exptparamstext,'FeatureNames'));


ExptParamsData = [];
for ept = 1:feature_names/2-1
    if strcmpi(exptparamstext{ept*2}(1),'D') %add colon back in
        if strcmpi(exptparamstext{ept*2}(1:3),'D\\') %add colon back in
            str = erase(exptparamstext{ept*2},',');
            ExptParamsData.(exptparamstext{ept*2-1}) = ['D:\\' str(4:end)];
        else
            ExptParamsData.(exptparamstext{ept*2-1}) = erase(exptparamstext{ept*2},',');
        end
    elseif strcmpi('TrialSequenceEpochs',exptparamstext{ept*2-1})
        str = strsplit(exptparamstext{ept*2},',');
        if isempty(str{end})
            ExptParamsData.(exptparamstext{ept*2-1}) = str(1:end-1);
        else
            ExptParamsData.(exptparamstext{ept*2-1}) = str;
        end
    else
        ExptParamsData.(exptparamstext{ept*2-1}) = erase(exptparamstext{ept*2},',');
    end
end

Feature_text = exptparamstext(feature_names+1:end);
ExptParamsData.FeatureNames.ShapeNames = Feature_text(find(contains(Feature_text,'S')));
ExptParamsData.FeatureNames.PatternNames = Feature_text(find(contains(Feature_text,'P')));
ExptParamsData.FeatureNames.ColorNames = Feature_text(find(contains(Feature_text,'C')));
ExptParamsData.FeatureNames.TextureNames = Feature_text(find(contains(Feature_text,'T')));
ExptParamsData.FeatureNames.ArmNames = Feature_text(find(contains(Feature_text,'A')));
end