%Written by Seth Koenig 4/22/19
%Reads in weight and water consumption charts

clar
log_dir = 'Z:\NHP Training\';

% excel_file_name = 'Bard_Sindri_WeeklyWaterLogs.xlsx';
% sheet_names = {'Sindri','Bard'};
% 
excel_file_name = 'Frey_Wotan_WeeklyWaterLogs.xlsx';
sheet_names = {'Frey19','Wotan19'};


% excel_file_name = 'Igor_Reider_WeeklyWaterLogs.xlsx';
% sheet_names = {'Igor','Reider'};



excel_data = cell(3,length(sheet_names));

for sheet = 1:length(sheet_names)
    [excel_data{1,sheet}, excel_data{2,sheet}, excel_data{3,sheet}] = xlsread([log_dir excel_file_name],sheet_names{sheet});
end

%% Read Data and Weights and Total Water

baseline_weight = NaN(1,length(sheet_names));
recomended_water = NaN(1,length(sheet_names));
for sheet = 1:length(sheet_names)
    recomended_water(sheet) = excel_data{1,sheet}(1,2);
    baseline_weight(sheet) = excel_data{1,sheet}(1,1);
end

%%
all_weekday = cell(1,length(sheet_names));
all_dates = cell(1,length(sheet_names));
all_weights = cell(1,length(sheet_names));
all_water_intake = cell(1,length(sheet_names));
for sheet = 1:length(sheet_names)
    %---Get Data Format---%
    [date_row,date_column] = find(contains(excel_data{2,sheet},'Date'));
    [weight_row,weight_column] = find(contains(excel_data{2,sheet},'Weight'));
    if length(weight_row) > 1
        if weight_row(1) == 1
            weight_row(1) = [];
            weight_column(1) = [];
        else
            error('Weird Weight ROw format');
        end
    end
    [water_intake_row,water_intake_column] = find(contains(excel_data{2,sheet},'Daily Total (ml)'));
    if weight_row ~= date_row || date_row ~= water_intake_row
        error('Unexpected Data row formatting');
    else
        first_date_row =  date_row+1;
    end
    
    %---Import Data---%
    num_rows = size(excel_data{3,sheet}(:,1),1);
    all_dates{sheet} = NaN(num_rows,3);
    all_weekday{sheet} = NaN(1,num_rows);
    all_weights{sheet} = NaN(1,num_rows);
    all_water_intake{sheet} = NaN(1,num_rows);
    for row = first_date_row:num_rows
        if ~isempty(excel_data{3,sheet}{row,date_column})
            datestr = excel_data{3,sheet}{row,date_column};
            dashes = strfind(datestr,'/');
            if isempty(dashes)
                continue
            elseif length(dashes) ~= 2
                error('Unkown date format')
            end
            all_dates{sheet}(row,1) = str2double(datestr(1:dashes(1)-1));%month
            all_dates{sheet}(row,2) = str2double(datestr(dashes(1)+1:dashes(2)-1));%day
            all_dates{sheet}(row,3) = str2double(datestr(dashes(2)+1:end));%year
            
            daystr = excel_data{3,sheet}{row,date_column+1};
            if strcmpi(daystr,'Monday')
                all_weekday{sheet}(row)= 1;
            elseif strcmpi(daystr,'Tuesday')
                all_weekday{sheet}(row)= 2;
            elseif strcmpi(daystr,'Wednesday')
                all_weekday{sheet}(row)= 3;
            elseif strcmpi(daystr,'Thursday')
                all_weekday{sheet}(row)= 4;
            elseif strcmpi(daystr,'Friday')
                all_weekday{sheet}(row)= 5;
            elseif strcmpi(daystr,'Saturday')
                all_weekday{sheet}(row)= 6;
            elseif strcmpi(daystr,'Sunday')
                all_weekday{sheet}(row)= 7;
            else
                error('Unknown Weekday')
            end
            
            all_weights{sheet}(row) =  excel_data{3,sheet}{row,weight_column};
            all_water_intake{sheet}(row) = excel_data{3,sheet}{row,water_intake_column};
        end
    end
end
%% Get unique all dates, kind of convoluted but should work if not weighted on same days

all_combined_dates = [];
for sheet = 1:length(sheet_names)
    all_combined_dates = [all_combined_dates; all_dates{sheet}];
end
all_combined_dates(isnan(all_combined_dates(:,1)),:) = [];

compact_num = 10000*all_combined_dates(:,3)+ 100*all_combined_dates(:,1) +  all_combined_dates(:,2);
[unqiue_dates,unique_ind] = unique(compact_num);

full_unique_dates = all_combined_dates(unique_ind,:);

all_date_ind = cell(1,length(sheet_names));
for sheet = 1:length(sheet_names)
    all_date_ind{sheet} = NaN(1,size(all_dates{sheet},1));
    for row = 1:size(all_dates{sheet},1)
        if ~isnan(all_dates{sheet}(row,1))
            compact = 10000*all_dates{sheet}(row,3)+ 100*all_dates{sheet}(row,1) +  all_dates{sheet}(row,2);
            all_date_ind{sheet}(row) = find(compact == unqiue_dates);
        end
    end
end


%%
figure
subplot(2,2,1)
hold on
for sheet = 1:length(sheet_names)
    these_weights =  all_weights{sheet};
    naninds = find(isnan(these_weights));
    these_weights(naninds) = [];
    these_dates = all_date_ind{sheet};
    these_dates(naninds) = [];
    plot(datetime(full_unique_dates(these_dates,3),full_unique_dates(these_dates,1),...
        full_unique_dates(these_dates,2)),these_weights)
    plot([datetime(full_unique_dates(1,3),full_unique_dates(1,1),full_unique_dates(1,2)),...
        datetime(full_unique_dates(end,3),full_unique_dates(end,1),full_unique_dates(end,2))],...
        [baseline_weight(sheet) baseline_weight(sheet)],'--')
end
xlim([datetime(full_unique_dates(1,3),full_unique_dates(1,1),full_unique_dates(1,2)),...
    datetime(full_unique_dates(end,3),full_unique_dates(end,1),full_unique_dates(end,2))]);
hold off
box off
xtickformat('dd-MMM-yyyy')
ylabel('Weight (Kg)')
legend({sheet_names{1},[sheet_names{1} ': Reference Weight'],...
    sheet_names{2},[sheet_names{2} ': Reference Weight']});
title('Weights')

subplot(2,2,2)
hold on
for sheet = 1:length(sheet_names)
    these_waters =  all_water_intake{sheet};
    naninds = find(isnan(these_waters) | these_waters == 0);
    these_waters(naninds) = [];
    these_dates = all_date_ind{sheet};
    these_dates(naninds) = [];
    plot(datetime(full_unique_dates(these_dates,3),full_unique_dates(these_dates,1),...
        full_unique_dates(these_dates,2)),these_waters)
    plot([datetime(full_unique_dates(1,3),full_unique_dates(1,1),full_unique_dates(1,2)),...
        datetime(full_unique_dates(end,3),full_unique_dates(end,1),full_unique_dates(end,2))],...
        [recomended_water(sheet) recomended_water(sheet)],'--')
end
xlim([datetime(full_unique_dates(1,3),full_unique_dates(1,1),full_unique_dates(1,2)),...
    datetime(full_unique_dates(end,3),full_unique_dates(end,1),full_unique_dates(end,2))]);
hold off
box off
xtickformat('dd-MMM-yyyy')
ylabel('Water (mL)')
legend({sheet_names{1},[sheet_names{1} ': Reference Water'],...
    sheet_names{2},[sheet_names{2} ': Reference Water']});
title('Water Intake')

subplot(2,2,3)
hold on
for sheet = 1:length(sheet_names)
    these_waters =  all_water_intake{sheet};
    naninds = find(isnan(these_waters) | these_waters == 0);
    these_waters(naninds) = [];
    these_waters = filtfilt(1/3*ones(1,3),1,these_waters);
    
    these_dates = all_date_ind{sheet};
    these_dates(naninds) = [];
    plot(datetime(full_unique_dates(these_dates,3),full_unique_dates(these_dates,1),...
        full_unique_dates(these_dates,2)),these_waters)
    plot([datetime(full_unique_dates(1,3),full_unique_dates(1,1),full_unique_dates(1,2)),...
        datetime(full_unique_dates(end,3),full_unique_dates(end,1),full_unique_dates(end,2))],...
        [recomended_water(sheet) recomended_water(sheet)],'--')
end
xlim([datetime(full_unique_dates(1,3),full_unique_dates(1,1),full_unique_dates(1,2)),...
    datetime(full_unique_dates(end,3),full_unique_dates(end,1),full_unique_dates(end,2))]);
hold off
box off
xtickformat('dd-MMM-yyyy')
ylabel('Water (mL)')
legend({sheet_names{1},[sheet_names{1} ': Reference Water'],...
    sheet_names{2},[sheet_names{2} ': Reference Water']});
title('Smoothed Water Intake')
%%
figure
hold on
for sheet = 1:length(sheet_names)
    these_waters =  all_water_intake{sheet};
    naninds = find(isnan(these_waters) | these_waters == 0);
    these_waters(naninds) = [];    
    these_days = all_weekday{sheet};
    these_days(naninds) = [];

    day_average = NaN(1,7);

    for dy = 1:7
        day_average(dy) = mean(these_waters(these_days == dy));
    end
    plot(1:7,day_average)
    
    plot([0 8],[recomended_water(sheet) recomended_water(sheet)],'--')
end
xlim([1 7])
hold off
box off
ylabel('Water (mL)')
legend({sheet_names{1},[sheet_names{1} ': Reference Water'],...
    sheet_names{2},[sheet_names{2} ': Reference Water']},'Location','NorthEastOutside');
title('Weekday Water Intake')