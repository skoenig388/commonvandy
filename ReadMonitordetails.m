function [pixelsize,cmsize] = ReadMonitordetails(monitor_details)
%writen by Seth Koenig 3/6/19 to quickly pares monitor details (screen dimensions)

monitor_details = strsplit(monitor_details);
monitor_details = cellfun(@(s) erase(s,':'),monitor_details, 'UniformOutput', false);
monitor_details = cellfun(@(s) erase(s,','),monitor_details, 'UniformOutput', false);
monitor_details = cellfun(@(s) erase(s,'['),monitor_details, 'UniformOutput', false);
monitor_details = cellfun(@(s) erase(s,']'),monitor_details, 'UniformOutput', false);
monitor_details = cellfun(@(s) erase(s,'{'),monitor_details, 'UniformOutput', false);
monitor_details = cellfun(@(s) erase(s,'}'),monitor_details, 'UniformOutput', false);

pixelsize = [str2double(monitor_details{2}) str2double(monitor_details{3})];
cmsize = [str2double(monitor_details{5}) str2double(monitor_details{6})];

end