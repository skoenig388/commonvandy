function BlockData = ReadBlockText(filename)

%---Use Low Level File I/O to read in data---%
fid = fopen(filename);
blocktext = textscan(fid,'%s');
fclose(fid);

%----Turn blocktext data into struepture----%
blocktext = blocktext{1};
blocktext = cellfun(@(s) erase(s,'"'), blocktext, 'UniformOutput', false);
blocktext = cellfun(@(s) erase(s,':'), blocktext, 'UniformOutput', false);
blocktext = cellfun(@(s) erase(s,'...'), blocktext, 'UniformOutput', false);
blocktext = cellfun(@(s) strtrim(s), blocktext, 'UniformOutput', false);

blockids = find(contains(blocktext,'BlockID'));
trialrange = find(contains(blocktext,'TrialRange'));

ActiveFeatureTemplate = find(contains(blocktext,'ActiveFeatureTemplate'));
ContextNums = find(contains(blocktext,'ContextNums'));
NumIrrelevantObjectsPerTrial = find(contains(blocktext,'NumIrrelevantObjectsPerTrial'));
NumIrrelevantObjectsPerBlock = find(contains(blocktext,'NumIrrelevantObjectsPerBlock'));
RuleArray = find(contains(blocktext,'RuleArray'));

trialrange1 = blocktext(trialrange+1);
trialrange1 = cellfun(@(s) erase(s,'['), trialrange1, 'UniformOutput', false);
trialrange1 = cellfun(@(s) erase(s,','), trialrange1, 'UniformOutput', false);
trialrange1 = cellfun(@str2double,trialrange1);
trialrange2 = blocktext(trialrange+2);
trialrange2 = cellfun(@(s) erase(s,']'), trialrange2, 'UniformOutput', false);
trialrange2 = cellfun(@(s) erase(s,','), trialrange2, 'UniformOutput', false);
trialrange2 = cellfun(@str2double,trialrange2);

BlockData.blockid = blocktext(blockids+1);
BlockData.trialrange = [trialrange1 trialrange2];

end