function number_of_irrelevant_dimensions = ParseBlockDef4NumIrrel(Blockdef)
%written by Seth Koenig 4/18/19
%Code Determinse the number of irrelevant features dimensions for each
%block and double checks that the blockdef names matche the number of
%irrelevant feature dimensions determined from the actual data. 

num_blocks = length(Blockdef);

number_of_irrelevant_dimensions = NaN(1,num_blocks);
for block = 1:num_blocks
    this_block_def = Blockdef(block);
    num_dims = sum( this_block_def.TrialDefs(1).relevantObjects(1).StimDimVals ~= 0);
    if all(this_block_def.RuleArray.RelevantFeatureTemplate == 0) %probably a hard coded block so for now assume 1 rel dim
        num_rule_dims = 1;
    else
        num_rule_dims = sum(this_block_def.RuleArray.RelevantFeatureTemplate ~= -1);
    end
    number_of_irrelevant_dimensions(block) = num_dims-num_rule_dims;
    
    if contains(this_block_def.BlockID,'Irrel') || contains(this_block_def.BlockID,'irrel') && ~ contains(this_block_def.BlockID,'First')
        if  contains(this_block_def.BlockID,'irrel')
            name_num_irrel = str2double(this_block_def.BlockID(strfind(this_block_def.BlockID,'irrel')-1)); 
            if isnan(name_num_irrel)
                name_num_irrel = str2double(this_block_def.BlockID(strfind(this_block_def.BlockID,'irrel')-2));
            end
        elseif  contains(this_block_def.BlockID,'Irrel')
            name_num_irrel = str2double(this_block_def.BlockID(strfind(this_block_def.BlockID,'Irrel')-1));
            if isnan(name_num_irrel)
                name_num_irrel = str2double(this_block_def.BlockID(strfind(this_block_def.BlockID,'Irrel')-2));
            end
        end
        if name_num_irrel ~= number_of_irrelevant_dimensions(block)
            error('Why is there a name and number of irrel mismatch?')
        end
    end
end
end