function [target_dim,target_feature_value] = DetermineTargetDimension(session_data,block)
%Written by Seth Koenig 6/24/19
%Determines target dimenions, easy if RuleArray is specified, harder if not

%Inputs:
%1) session_data: session data from single session
%2) block: block number to determine target dimension

%Outputs:
%1) target_dim: target dimensions
%2) target_feature_value: target feature value

if all(session_data.block_def(block).RuleArray.RelevantFeatureTemplate == 0)
    num_trials = length(session_data.block_def(block).TrialDefs);
    all_rewarded_values = NaN(num_trials,5);
    for t = 1:num_trials
        if session_data.block_def(block).TrialDefs(1).relevantObjects(1).StimTrialRewardProb == 1
            all_rewarded_values(t,:) = session_data.block_def(block).TrialDefs(t).relevantObjects(1).StimDimVals;
        else
            error('Why isnt the first stimulus rewarded?')
        end
    end
    num_unique_vals = NaN(1,5);
    for dim = 1:5
        if all(all_rewarded_values(:,dim) ~= 0)
            num_unique_vals(dim) = length(unique(all_rewarded_values(:,dim)));
        end
    end
    target_dim = find(num_unique_vals == 1);
    if isempty(target_dim)
        error('Uknonwn target dimension')
    end
    target_feature_value = all_rewarded_values(1,target_dim);
else
    target_dim = find(session_data.block_def(block).RuleArray.RelevantFeatureTemplate ~= - 1);
    if isempty(target_dim)
        error('Uknonwn target dimension')
    end
    target_feature_value = session_data.block_def(block).RuleArray.RelevantFeatureTemplate(target_dim);
end