%Rename files if forgot to put subject name


direct = 'Z:\DATA_kiosk\Igor\Feature Search Task\FLU__Igor__12_02_2019__10_26_44\RuntimeData\TrialData\';

oldname = 'FLU_';
newsubjname = 'FS_';
files = dir(direct);

for f = 1:length(files)
    if ~files(f).isdir
        if contains(files(f).name,'txt') || contains(files(f).name,'json')
            if contains(files(f).name,oldname)
                subname = strfind(files(f).name,oldname);
                newname = [files(f).name(1:subname-1) newsubjname files(f).name(subname+length(oldname):end)];
                movefile([direct files(f).name],[direct newname])
            end
        end
    end
end