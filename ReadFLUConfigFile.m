function configdata = ReadFLUConfigFile(filename)

%---Use Low Level File I/O to read in data---%
fid = fopen(filename);
line = 0;
configtext = {};
ln = 1;
while line ~= - 1
    line = fgetl(fid);
    if isempty(line)
        line = 0;
        continue
    end
    
    if length(line) == 1
        if line == -1
            continue
        end
    end
    
    if contains(line,'//') %%contains commented info
        slashes =  strfind(line,'//');
        if length(slashes) > 1
            continue
        elseif slashes < 5
            continue
        else
            disp('Found // in Config File but dont know what to do with them')
        end
    end
    configtext{ln} = line;
    ln = ln+1;
end
fclose(fid);

%----Turn configtext data into structure----%
configtext = cellfun(@(s) strsplit(s, '\t'), configtext, 'UniformOutput', false);
configtext = [configtext{:}];
empts = find(cellfun(@isempty,configtext));
configtext(empts) = [];
configtext(1:3:end) = [];%variable type which is not necessary
configtext = cellfun(@(s) erase(s,'"'), configtext, 'UniformOutput', false);
%configtext = [configtext(1:2:end)' configtext(2:2:end)'];
%%configdata = cell2table(configtext,'VariableNames',{'ConfigVariable','ConfigValue'})

configdata = [];
for ct = 1:length(configtext)/2
   if strcmpi(configtext{ct*2-1},'2Dview')
          configdata = setfield(configdata,'View2D',configtext{ct*2});
   elseif strcmpi(configtext{ct*2-1},'2DviewNumLocations')
          configdata = setfield(configdata,'View2DNumLocations',configtext{ct*2});
   else
          configdata = setfield(configdata,configtext{ct*2-1},configtext{ct*2});
   end
end

end