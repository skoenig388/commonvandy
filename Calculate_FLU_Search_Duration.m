function search_duration = Calculate_FLU_Search_Duration(session_data)
%adopted from other code, Seth Koenig 5/8/19
%meant to be in one place so can modify or debug easily

search_start_time = session_data.trial_data.Epoch3_StartTimeRelative+session_data.trial_data.baselineDuration;
search_end_time =  session_data.trial_data.Epoch7_StartTimeRelative...
    -session_data.trial_data.Epoch6_Duration-session_data.trial_data.Epoch5_Duration;
search_duration = search_end_time-search_start_time;