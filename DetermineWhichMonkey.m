function [which_monkey,monkey_name]= DetermineWhichMonkey(filename)
%Written by Seth Koenig 3/8/19
%do this over and over again across task and it takes up too much room so
%thought I would make it a 1 liner everywhere else
if contains(filename,'Bard')
    which_monkey = 1;
    monkey_name = 'Bard';
elseif contains(filename,'Frey')
    which_monkey = 2;
    monkey_name = 'Frey';
elseif contains(filename,'Igor')
    which_monkey = 3;
    monkey_name = 'Igor';
elseif contains(filename,'Reider')
    which_monkey = 4;
    monkey_name = 'Reidor';
elseif contains(filename,'Sindri')
    which_monkey = 5;
    monkey_name = 'Sindri';
elseif contains(filename,'Wotan')
    which_monkey = 6;
    monkey_name = 'Wotan';
elseif contains(filename,'MFLU')
    which_monkey = 0;%for human
    monkey_name = 'MFLU';
else
    error('Name Not found')
end

end