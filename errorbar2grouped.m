function errorbar2grouped(means,sems,sb1,sb2,sbn)
%because errorbar doesn't work with grouped plots, decided to create
%function to do this instead. Code based on code found online here:
%https://www.mathworks.com/matlabcentral/answers/102220-how-do-i-place-errorbars-on-my-grouped-bar-graph-using-function-errorbar-in-matlab
%only output is plot

%Inputs:
%   1) means: Mean values
%   2) sems: standard error or error values
%   3) sb1: number subplot rows
%   4) sb2: number subplot columns
%   5) sbn: subplot number
%
%Outputs: just a plot

if sb1 == 1 && sb2 == 1
    figure
else
    subplot(sb1,sb2,sbn)
end

h = bar(means','BarWidth',1);
hold on;
ngroups = size(means, 2);
nbars = size(means, 1);
groupwidth = min(0.8, nbars/(nbars + 1.5));
for i = 1:nbars
    x = (1:ngroups) - groupwidth/2 + (2*i-1) * groupwidth / (2*nbars);
    errorbar(x, means(i,:), sems(i,:), 'k', 'linestyle', 'none');
end
hold off
end