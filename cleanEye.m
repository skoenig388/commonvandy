function [clean_x,clean_y] = cleanEye(eye_x,eye_y,screenX,screenY,removeNaN)
%written by Seth Koenig, 3/21/19

if nargin == 2
    screenX = 1920;
    screenY = 1080;
    removeNaN = 2;
elseif nargin == 4
    removeNaN = 2;
end

if removeNaN == 1 %remove unwanted values
    bad_ind = find(eye_x < 1 | eye_x > screenX | isnan(eye_x) | eye_y < 1 | eye_y > screenY | isnan(eye_y));
    eye_x(bad_ind) = [];
    eye_y(bad_ind) = [];
else %replace values with NaNs
    bad_ind = find(eye_x < 1 | eye_x > screenX | eye_y < 1 | eye_y > screenY);
    eye_x(bad_ind) = NaN;
    eye_y(bad_ind) = NaN;
end

clean_x = eye_x;
clean_y = eye_y;

end