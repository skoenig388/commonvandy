function datatbl = ImportUDPRecv(filename,import_opts)

%Written by Seth Konig 10/31/18

%%
try
    import_opts.SelectedVariableNames = {'Val2','Var3','Var6','Var8','ExtraVar3','ExtraVar7'};
    datatbl = readtable(filename,import_opts);
    datatbl.Properties.VariableNames = {'Frame','FrameStartUnity','X','Y','EyeTrackerTimeStamp','PythonReceivedFrame','PythonTimeStamp'};
catch
    %cheap and dirty way of importing UPD data
    fid = fopen(filename);
    data = NaN(1000,7);
    count = 0;
    linecount = 0;
    line = 1;
    while line ~= -1
        line = fgetl(fid);
        if line == -1
            continue
        end
        if isempty(line)
            line = 1;
            continue
        end
        count = count+1;
        if count == 1
            continue
        end
        linecount = linecount+1;
        C =   strsplit(line,{'\t',' ','###'});
        Cwidth = length(C);
        if Cwidth <= 3
            continue
        elseif Cwidth == 4 %no message
            data(linecount,1) = str2double(C{2});%frame #
            data(linecount,2) = str2double(C{3});%frame start time unity
        elseif Cwidth == 13 || Cwidth == 10 || Cwidth == 7 || Cwidth == 8%not sure what do do with this yet, but they're calibration messages
            data(linecount,1) = str2double(C{2});%frame #
            data(linecount,2) = str2double(C{3});%frame start time unity
            
            data(linecount,3) = str2double(C{6});%X
            try
                data(linecount,4) = str2double(C{8});%Y
            catch
                disp('Missing Eye Data')
            end
        elseif Cwidth == 18% gaze data in the message
            data(linecount,1) = str2double(C{2});%frame #
            data(linecount,2) = str2double(C{3});%frame start time unity
            
            data(linecount,3) = str2double(C{6});%X
            data(linecount,4) = str2double(C{8});%Y
            
            data(linecount,5) = str2double(C{14});%Eye tracker time stamp
            data(linecount,6) = str2double(C{16});%Python's Received Frame
            
            data(linecount,7) = str2double(C{18});%Python's Time stamp
        else
            error(['Message contains ' num2str(Cwidth) ', unkown format'])
        end
    end
    fclose(fid);
    data = laundry(data);
    
    if ~isempty(data)
        datatbl = table(data(:,1),data(:,2),data(:,3),data(:,4),data(:,5),data(:,6), data(:,7),...
            'VariableNames',{'Frame','FrameStartUnity','X','Y','EyeTrackerTimeStamp','PythonReceivedFrame','PythonTimeStamp'});
    else
        disp(['Couldnt propperly import' filename])
        datatbl = [];
    end
end