function datatbl = ImportPythonUDPSent(filename,import_opts)
%imports Python UDP Sent data
%Written by Seth Konig 2/15/19

try
    import_opts.SelectedVariableNames = {'Var3','Var5','Var7','Var9','Var11','Var13','Var15'};
    datatbl = readtable(filename,import_opts);
    datatbl.Properties.VariableNames = {'X','Y','ValL','ValR','EyeTrackerTimeStamp','PythonRecvFrame','PythonTimeStamp'};
catch
    disp(['Could not properly import ' filename]);
    datatbl = [];
end

end