function all_session_data = ImportFLU_DataV2(filename,log_dir,import_option,save_data)
%Seth Koenig 4/11/19; updated from ImportFLU_Data to be faster and quicker
%code imports all the data from the FLU task as well as similar tasks.

%Inputs:
%1) filename: file name to be imported
%2) log_dir: location of data directory where filename data is stored
%3) import_options
%   a) all: imports all data
%   b) no_gaze: doesn't import gaze data but imports everything else
%   c) slim: imports only trial data: no frame and no eye data
%4) save_data: if true, saves imported data to log_dir under '\Processed_Data\' folder

%Outputs:
%1) all_session_data: imported data

if nargin < 3
    import_option = 'all';
    save_data = false;
elseif nargin < 4
    save_data = false;
end

%% Define File Paths
trial_dir = [log_dir filename '\RuntimeData\TrialData\'];
trial_name = '__TrialData.txt';
trialdef_name = '__trialdef_on_trial_';

config_dir = [log_dir filename '\RuntimeData\ConfigCopy\'];
block_name = '__block_defs.json';

frame_data_dir = [log_dir filename '\RuntimeData\FrameData\' filename];
frame_data_name = '__FrameData__Trial_';

UDP_recv_data_dir = [log_dir filename '\RuntimeData\UnityUDPRecvData\' filename];
UDP_sent_data_dir = [log_dir filename '\RuntimeData\UnityUDPSentData\' filename];
UDP_sent_name = '__UDPSent__Trial_';
UDP_recv_name = '__UDPRecv__Trial_';

PythonUDP_Recv_data = [log_dir filename '\RuntimeData\PythonData\PythonUDPRecvData\' filename];
PythonUDP_Sent_data = [log_dir filename '\RuntimeData\PythonData\PythonUDPSentData\' filename];
PythonUDP_sent_name = '__UDPSentData__Trial_';
PythonUDP_recv_name = '__UDPRecvData__Trial_';

gaze_data_dir = [log_dir filename '\RuntimeData\PythonData\GazeData\'];
gaze_data_name = '__GazeData__Trial_';


%% Load Config Variables
block_json = ReadJsonFile([config_dir filename '__block_defs.json']);
list = dir(config_dir);
for l = 1:length(list)
    if list(l).isdir
        continue
    else
        if contains(list(l).name,'BlockDef') && contains(list(l).name,'.txt')
            %still need to read in data correctly for this but got block_json for now
            blockData =  ReadBlockText([config_dir list(l).name]);
        elseif contains(list(l).name,'ExptParameters')
            ExptParamsData = ReadExperimentalParameters([config_dir list(l).name]);
            num_epochs = length(ExptParamsData.TrialSequenceEpochs);
        elseif contains(list(l).name,'config') && contains(list(l).name,'session')
            configdata = ReadFLUConfigFile([config_dir list(l).name]);
        end
    end
end

%% Load Trial and Frame Data
trial_data = readtable([trial_dir filename trial_name]);
if trial_data(end,:).Epoch4_Duration == -1
    %last trial was never completed
    trial_data(end,:) = [];
end
block_def = ReadJsonFile([config_dir filename block_name]);

num_trials = size(trial_data,1);
if num_trials == 0
    num_trials = 1;
end
TrialDefs = cell(1,num_trials);
frame_data = [];
num_frame_data_files = size(ls(frame_data_dir(1:end-length(filename))),1);
if num_frame_data_files <= 4 %old version where 1 frame data file for all trials
    if ~strcmpi(import_option,'slim')
        frame_data = readtable([frame_data_dir frame_data_name(1:end-8) '.txt'],'Delimiter','\t');
    end
    for t = 1:num_trials
        TrialDefs{t} = ReadJsonFile([trial_dir filename trialdef_name num2str(t) '.json']);
    end
else
    frame_data = cell(1,num_trials);
    %--Getting importing options here leads to about 50% speed up compared
    %to trying to do it 1000 times just better to define once
    frame_import_opts = detectImportOptions([frame_data_dir frame_data_name num2str(1) '.txt']);
    if length(frame_import_opts.Delimiter) > 1
        error('Found too many delimeters')
    elseif ~strcmpi(frame_import_opts.Delimiter{1},'\t')
        error('Wrong Delimeter Identified')
    end
    for t = 1:num_trials
        TrialDefs{t} = ReadJsonFile([trial_dir filename trialdef_name num2str(t) '.json']);
        if ~strcmpi(import_option,'slim')
            frame_data{t} = readtable([frame_data_dir frame_data_name num2str(t) '.txt'],frame_import_opts);
            if ~iscell(frame_data{t}.TouchedObjectId)
                if all(isnan(frame_data{t}.TouchedObjectId))
                    frame_data{t}.TouchedObjectId = cell(length(frame_data{t}.TouchedObjectId),1);
                else
                    error('TouchedObjectID format unknown')
                end
            end
        end
    end
    frame_data = vertcat(frame_data{:});
end

%% Read in eye tracker related data
UDP_recv_data = cell(1,num_trials); %UDP Unity Receive Data
UDP_sent_data = cell(1,num_trials); %UDP Unity Sent Data
pythonUDP_sent_data = cell(1,num_trials); %Python UDP Sent Data
pythonUDP_recv_data = cell(1,num_trials); %Python UDP Recv Data
gaze_data = cell(1,num_trials); %Python Stored Gaze Data at up to 600 Hz
average_participant_distance = [];
if strcmpi(import_option,'all')
    if exist(UDP_recv_data_dir(1:end-length(filename)),'dir')
        if num_frame_data_files <= 4
            UDP_recv_data = ImportUDPRecv([UDP_recv_data_dir UDP_recv_name(1:end-8) '.txt']);
            UDP_sent_data = ImportUDPsent([UDP_sent_data_dir  UDP_recv_name(1:end-8) '.txt']);
        else
            gaze_import_opts = detectImportOptions([gaze_data_dir filename gaze_data_name  num2str(t) '.txt']);
            if length(gaze_import_opts.Delimiter) > 1
                error('Found too many delimeters')
            elseif ~strcmpi(gaze_import_opts.Delimiter{1},'\t')
                error('Wrong Delimeter Identified')
            end
            gaze_import_opts.SelectedVariableNames = {'system_time_stamp','device_time_stamp',...
                'left_ADCS_validity','right_ADCS_validity','left_ADCS_x','left_ADCS_y',...
                'right_ADCS_x','right_ADCS_y','left_pupil_diameter','right_pupil_diameter',...
                'left_origin_UCS_z','right_origin_UCS_z'};
            
            UDP_recv_import_opts = detectImportOptions([UDP_recv_data_dir UDP_recv_name num2str(t) '.txt']);
            if length(UDP_recv_import_opts.Delimiter) < 2
                if strcmpi(UDP_recv_import_opts.Delimiter{1},'\t')
                    UDP_recv_import_opts.Delimiter{2} = ' ';
                else
                    error('Found too many delimeters')
                end
            elseif ~strcmpi(UDP_recv_import_opts.Delimiter{1},'\t')
                error('Wrong Delimeter Identified')
            end
            
            UDP_sent_import_opts = detectImportOptions([UDP_sent_data_dir  UDP_sent_name num2str(t) '.txt']);
            if length(UDP_sent_import_opts.Delimiter) > 1
                error('Found too many delimeters')
            elseif ~strcmpi(UDP_sent_import_opts.Delimiter{1},'\t')
                error('Wrong Delimeter Identified')
            end
            
            Python_UDP_recv_import_opts = detectImportOptions([PythonUDP_Recv_data PythonUDP_recv_name num2str(t) '.txt']);
            if length(Python_UDP_recv_import_opts.Delimiter) > 2
                error('Found too many delimeters')
            elseif ~strcmpi(Python_UDP_recv_import_opts.Delimiter{1},'\t')
                error('Wrong Delimeter Identified')
            elseif ~strcmpi(Python_UDP_recv_import_opts.Delimiter{2},' ')
                error('Wrong Delimeter Identified')
            end
            
            Python_UDP_sent_import_opts = detectImportOptions([PythonUDP_Sent_data PythonUDP_sent_name num2str(t) '.txt']);
            if length(Python_UDP_sent_import_opts.Delimiter) > 2
                error('Found too many delimeters')
            elseif ~strcmpi(Python_UDP_sent_import_opts.Delimiter{1},'\t')
                error('Wrong Delimeter Identified')
            elseif ~strcmpi(Python_UDP_sent_import_opts.Delimiter{2},' ')
                error('Wrong Delimeter Identified')
            end
            
            for t = 1:num_trials
                %Read in UDP Files
                UDP_recv_data{t} = ImportUDPRecv([UDP_recv_data_dir UDP_recv_name num2str(t) '.txt'],UDP_recv_import_opts);
                UDP_sent_data{t} = ImportUDPsent([UDP_sent_data_dir UDP_sent_name num2str(t) '.txt'],UDP_sent_import_opts);
                
                if exist(gaze_data_dir,'dir')
                    pythonUDP_recv_data{t} = ImportPythonUDPRecv([PythonUDP_Recv_data PythonUDP_recv_name num2str(t) '.txt'],Python_UDP_recv_import_opts);
                    pythonUDP_sent_data{t} = ImportPythonUDPSent([PythonUDP_Sent_data PythonUDP_sent_name num2str(t) '.txt'],Python_UDP_sent_import_opts);
                    
                    %Read in Gaze Data Files
                    if exist([gaze_data_dir filename gaze_data_name  num2str(t) '.txt'],'file')
                        temp_data = readtable([gaze_data_dir filename gaze_data_name  num2str(t) '.txt'],gaze_import_opts);
                        temp_data = clean_up_temp_data(temp_data); %check to make sure there are no-cells
                        
                        gaze_data{t} = temp_data;
                        average_participant_distance(t) = nanmean(temp_data.left_origin_UCS_z+temp_data.right_origin_UCS_z)/2;
                    else
                        disp(['Gaze Data file for ' filename ', trial #' num2str(t) ' is missing!!! Could be due to Calibration']);
                        continue
                    end
                end
            end
        end
    end
end
UDP_recv_data = vertcat(UDP_recv_data{:});
UDP_sent_data = vertcat(UDP_sent_data{:});
pythonUDP_sent_data = vertcat(pythonUDP_sent_data{:});
pythonUDP_recv_data =vertcat(pythonUDP_recv_data{:});
gaze_data = vertcat(gaze_data{:});
if ~isempty(gaze_data)
    %rename names so that you can process them later
    gaze_data.Properties.VariableNames = {'PythonTimeStamp','EyeTrackerTimeStamp','ValL','ValR',...
        'Lx','Ly','Rx','Ry','PupilL','PupilR','LeftDist','RightDist'};
end
%% Organize Data in to one Structure
all_session_data.filename = filename;
all_session_data.log_dir = log_dir;
all_session_data.num_trials = num_trials;
all_session_data.ExptParamsData = ExptParamsData;
all_session_data.num_epochs = num_epochs;
all_session_data.configdata = configdata;
all_session_data.block_def = block_def;
all_session_data.blockjson = block_json;
all_session_data.blockData = blockData;
all_session_data.trial_data = trial_data;
all_session_data.trial_def = TrialDefs;
all_session_data.frame_data = frame_data;
all_session_data.UDP_recv_data = UDP_recv_data;
all_session_data.UDP_sent_data = UDP_sent_data;
all_session_data.pythonUDP_sent_data = pythonUDP_sent_data;
all_session_data.pythonUDP_recv_data = pythonUDP_recv_data;
all_session_data.gaze_data = gaze_data;
all_session_data.average_participant_distance = average_participant_distance;

if save_data
    pre_processed_data_dir = [log_dir filename '\Processed_Data\'];
    if ~exist(pre_processed_data_dir,'dir')
        mkdir(pre_processed_data_dir)
    end
    save([pre_processed_data_dir filename '_session_data.mat'],'all_session_data');
end
end

function table_to_clean = clean_up_temp_data(table_to_clean)
if iscell(table_to_clean.system_time_stamp)
    table_to_clean.system_time_stamp = cellfun(@(s) str2double(s),table_to_clean.system_time_stamp);
end
if iscell(table_to_clean.device_time_stamp)
    table_to_clean.device_time_stamp = cellfun(@(s) str2double(s),table_to_clean.device_time_stamp);
end
if iscell(table_to_clean.left_ADCS_validity)
    table_to_clean.left_ADCS_validity = cellfun(@(s) str2double(s),table_to_clean.left_ADCS_validity);
end
if iscell(table_to_clean.right_ADCS_validity)
    table_to_clean.right_ADCS_validity = cellfun(@(s) str2double(s),table_to_clean.right_ADCS_validity);
end
if iscell(table_to_clean.left_ADCS_x)
    table_to_clean.left_ADCS_x = cellfun(@(s) str2double(s),table_to_clean.left_ADCS_x);
end
if iscell(table_to_clean.left_ADCS_y)
    table_to_clean.left_ADCS_y = cellfun(@(s) str2double(s),table_to_clean.left_ADCS_y);
end
if iscell(table_to_clean.right_ADCS_x)
    table_to_clean.right_ADCS_x = cellfun(@(s) str2double(s),table_to_clean.right_ADCS_x);
end
if iscell(table_to_clean.right_ADCS_y)
    table_to_clean.right_ADCS_y = cellfun(@(s) str2double(s),table_to_clean.right_ADCS_y);
end
if iscell(table_to_clean.left_pupil_diameter)
    table_to_clean.left_pupil_diameter = cellfun(@(s) str2double(s),table_to_clean.left_pupil_diameter);
end
if iscell(table_to_clean.right_pupil_diameter)
    table_to_clean.right_pupil_diameter = cellfun(@(s) str2double(s),table_to_clean.right_pupil_diameter);
end
if iscell(table_to_clean.left_origin_UCS_z)
    table_to_clean.left_origin_UCS_z = cellfun(@(s) str2double(s),table_to_clean.left_origin_UCS_z);
end
if iscell(table_to_clean.right_origin_UCS_z)
    table_to_clean.right_origin_UCS_z = cellfun(@(s) str2double(s),table_to_clean.right_origin_UCS_z);
end
end