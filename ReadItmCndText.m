function [ItmData,CndData] = ReadItmCndText(filename)
ItmData = readtable([filename '_Itm.txt']);
CndData = readtable([filename '_CND.txt']);
end